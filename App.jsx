import React from "react";
import './App.css';
import { Login } from "./Login";
import { Register } from "./Register";
import { Routes, Route } from 'react-router-dom'
import { Welcome } from "./Welcome";


function App() {

  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path='/welcome' element={<Welcome />} />
      </Routes>
    </div>
  );
}

export default App;
