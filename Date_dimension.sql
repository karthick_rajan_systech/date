


create table Dates
(id int primary key not null,
Dates date,
year_id int
)


create procedure form_dates
as
declare @start_date date = '01-01-1962'
declare @end_date date = '12-31-2024'
declare @count int =0
declare @year int =1962
declare @year_id int = 1 
while @start_date <= @end_date
	begin
		insert into Dates values(@count+1,@start_date,@year_id)
		set @count = @count+1
		set @start_date = dateadd(day,1,@start_date)
		if @year <> year(@start_date)
			set @year_id = @year_id+1
			set @year = year(@start_date)
	end
exec form_dates
	select * from dates

	drop procedure form_dates

drop table dates
truncate table 


insert into Dim_year_SQL_IN1544
select distinct(year(Dates)) from Dates
order by year(Dates)

select * from Dim_year_SQL_IN1544

truncate table Dim_year_SQL_IN1544






insert into Dim_qtr_SQL_IN1544(quarter_id,year_key,year_id)
select distinct(datename(qq,d.dates)) as quater_id,y.year_key,y.year_id
from Dim_year_SQL_IN1544 y join dates d on y.year_key = d.year_id
order by y.year_id

select * from Dim_qtr_SQL_IN1544

drop table Dim_qtr_SQL_IN1544

truncate table Dim_qtr_SQL_IN1544

create table Dim_month_SQL_IN1544 (Month_key int identity(1,1) primary key not null,
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null)

truncate table Dim_month_SQL_IN1544

insert into  Dim_month_SQL_IN1544 (month_id,quarter_key,quarter_id,year_key,year_id)
select distinct(month(d.dates)) months,
q.quarter_key,
q.quarter_id,
q.year_key,
q.year_id
from dates d join Dim_qtr_SQL_IN1544 q on year(d.dates) = q.year_id and datepart(qq,d.dates) = q.quarter_id
order by year_id,months

select * from Dim_month_SQL_IN1544






insert into Dim_week_SQL_IN1544(week_id,Month_key,month_id,quarter_key,quarter_id,year_key
,year_id)
select distinct(datepart(ww,d.dates)),
m.Month_key,
m.month_id,
m.quarter_key,
m.quarter_id,
m.year_key,
m.year_id
from dates d join Dim_month_SQL_IN1544 m on year(d.dates) = m.year_id
and datepart(qq,d.dates) = m.quarter_id
and datepart(mm,d.dates) = m.month_id
order by year_id

select * from Dim_week_SQL_IN1544




insert into Dim_Day_SQL_IN1544
(
Date_id		,
Day_id		,
week_key	,
Week_id		,
Month_key	,
Month_id	,
Quarter_key	,
Quarter_id	,
year_key	,
year_id		)

select distinct(d.dates),datepart(dd,d.dates),
w.week_key		,
w.Week_id		,
w.Month_key		,
w.Month_id		,
w.quarter_key	,
w.quarter_id	,
w.year_key		,
w.year_id
from dates d join Dim_week_SQL_IN1544 w on year(d.dates) = w.year_id
and datepart(qq,d.dates) = w.quarter_id
and datepart(mm,d.dates) = w.month_id
and datepart(ww,d.dates) = w.week_id
order by year_id

select * from Dim_Day_SQL_IN1544




drop table dim_day
truncate table dim_day
select * from dim_day
order by date_id






select * from [BCMPWMT].[SALES_ORDER]

create table Dim_week_SQL_IN1544 (week_key int identity(1,1) primary key not null,
Week_id int not null,
Month_key int not null foreign key references Dim_month_SQL_IN1544(Month_key),
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null)

drop table Dim_week_SQL_IN1544

create table Dim_Day_SQL_IN1544 (Day_key int identity(1,1) primary key not null,
Date_id date not null,
Day_id int not null,
week_key int not null foreign key references Dim_week_SQL_IN1544(week_key),
Week_id int not null,
Month_key int not null foreign key references Dim_month_SQL_IN1544(Month_key),
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null)

drop table Dim_Day_SQL_IN1544

create table Dim_Day_hour_SQL_IN1544 (Day_hour_key int identity(1,1) primary key not null,
Day_key int not null foreign key references Dim_Day_SQL_IN1544(Day_key),
Date_id date not null,
Day_id int not null,
week_key int not null foreign key references Dim_week_SQL_IN1544(week_key),
Week_id int not null,
Month_key int not null foreign key references Dim_month_SQL_IN1544(Month_key),
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null,
hour_id int not null,
hour_key int)

drop table Dim_Day_hour_SQL_IN1544

select * from Dim_Day_hour_SQL_IN1544

insert into Dim_Day_hour_SQL_IN1544
(
Day_key,
Date_id		,
Day_id		,
week_key	,
Week_id		,
Month_key	,
Month_id	,
Quarter_key	,
Quarter_id	,
year_key	,
year_id,
hour_id)
select
t.Day_key,
t.Date_id		,
t.Day_id		,
t.week_key	,
t.Week_id		,
t.Month_key	,
t.Month_id	,
t.Quarter_key	,
t.Quarter_id	,
t.year_key	,
t.year_id,
s.hour_id
from  Dim_Day_SQL_IN1544 t cross join  hur s
order by date_id

update Dim_Day_hour_SQL_IN1544 set hour_key = Day_hour_key

drop table 

select * from 


create table hur(hour_id int, hour_key int)

drop table hur

select * from hur

insert into hur values (0,0)
insert into hur values (1,1)
insert into hur values (2,2)
insert into hur values (3,3)
insert into hur values (4,4)
insert into hur values (5,5)
insert into hur values (6,6)
insert into hur values (7,7)
insert into hur values (8,8)
insert into hur values (9,9)
insert into hur values (10,10)
insert into hur values (11,11)
insert into hur values (12,12)
insert into hur values (13,13)
insert into hur values (14,14)
insert into hur values (15,15)
insert into hur values (16,16)
insert into hur values (17,17)
insert into hur values (18,18)
insert into hur values (19,19)
insert into hur values (20,20)
insert into hur values (21,21)
insert into hur values (22,22)
insert into hur values (23,23)


create table Dim_year_SQL_IN1544(year_key	int identity(1,1) primary key not null,
year_id	int not null)

drop table Dim_year_SQL_IN1544

create table Dim_qtr_SQL_IN1544 (Quarter_key int identity(1,1) primary key not null,
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id	int not null)

drop table Dim_qtr_SQL_IN1544

create table Dim_month_SQL_IN1544 (Month_key int identity(1,1) primary key not null,
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null)

drop table Dim_month_SQL_IN1544

create table Dim_week_SQL_IN1544 (week_key int identity(1,1) primary key not null,
Week_id int not null,
Month_key int not null foreign key references Dim_month_SQL_IN1544(Month_key),
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null)

drop table Dim_week_SQL_IN1544

create table Dim_Day_SQL_IN1544 (Day_key int identity(1,1) primary key not null,
Date_id date not null,
Day_id int not null,
week_key int not null foreign key references Dim_week_SQL_IN1544(week_key),
Week_id int not null,
Month_key int not null foreign key references Dim_month_SQL_IN1544(Month_key),
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null)

drop table Dim_Day_SQL_IN1544

create table Dim_Day_hour_SQL_IN1544 (Day_hour_key int identity(1,1) primary key not null,
Day_key int not null foreign key references Dim_Day_SQL_IN1544(Day_key),
Date_id date not null,
Day_id int not null,
week_key int not null foreign key references Dim_week_SQL_IN1544(week_key),
Week_id int not null,
Month_key int not null foreign key references Dim_month_SQL_IN1544(Month_key),
Month_id int not null,
Quarter_key int not null foreign key references Dim_qtr_SQL_IN1544(Quarter_key),
Quarter_id int not null,
year_key int not null foreign key references Dim_year_SQL_IN1544(year_key),
year_id int not null,
hour_id int not null,
hour_key int not null)

drop table Dim_Day_hour_SQL_IN1544
