create table DIM_CUST_CNTCT_SQL_IN1544
(CUST_CNTCT_KEY int identity(1,1) primary key not null,
CNTCT_ID int not null,
TENANT_ORG_ID int not null,
SRC_CNTCT_ID varchar(50)not null,
DATA_SRC_ID int not null,
ACCT_ID int not null,
ADDR_ID bigint not null,
PHONE_ID bigint not null,
EMAIL_ID varchar(250) not null,
ADDR_ZONE_ID int not null,
DELTD_YN char(20) not null,
CRE_DT Date not null, 
UPD_TS nvarchar(255) not null)
cust_addr_zone_key int not null foreign key references dim_cust_addr_zone_SQL_IN1544(addr_zone_id_key) ON DELETE CASCADE,
cust_phone_key int not null foreign key references DIM_CUST_PHONE_SQL_IN1544(Cust_phone_key) ON DELETE CASCADE,
Cust_email_key int not null foreign key references DIM_CUST_EMAIL_SQL_IN1544(Cust_email_key) ON DELETE CASCADE,
ciust_accnt_key int not null foreign key references dim_CUST_ACCT_SQL_IN1544(cust_acct_key) ON DELETE CASCADE)



select * from BCMPWMT.CUST_CNTCT
select * from DIM_CUST_CNTCT_SQL_IN1544

insert into DIM_CUST_CNTCT_SQL_IN1544
select 
iif(CNTCT_ID is null or CNTCT_ID =  'NULL',101,cast(ltrim(rtrim(Cntct_id)) as int)) as CNTCT_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_org_id)) as int)) as TENANT_ORG_ID,
iif(SRC_CNTCT_ID is null or SRC_CNTCT_ID =  'NULL','N/A',ltrim(rtrim(SRC_CNTCT_ID))) as SRC_CNTCT_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(ACCT_ID is null or ACCT_ID =  'NULL',101,cast(ltrim(rtrim(Acct_id)) as int)) as ACCT_ID,
iif(ADDR_ID is null or ADDR_ID =  'NULL',101,cast(ltrim(rtrim(Addr_id)) as bigint)) as ADDR_ID,
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(Phone_id)) as bigint)) as PHONE_ID,
iif(EMAIL_ID is null or EMAIL_ID =  'NULL','N/A',ltrim(rtrim(Email_ID))) as EMAIL_ID,
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID =  'NULL',101,cast(ltrim(rtrim(Addr_Zone_Id))as int)) as ADDR_ZONE_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','Y',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date))  as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL',cast('01-01-1900' as varchar),cast(format(cast(UPD_TS as datetime),'dd/MMMM/yyyy')as varchar)) as UPD_TS
from BCMPWMT.CUST_CNTCT

ALTER TABLE DIM_CUST_CNTCT_SQL_IN1544 ADD CUST_PHONE_KEY INT
 UPDATE DIM_CUST_CNTCT_SQL_IN1544 SET DIM_CUST_CNTCT_SQL_IN1544.CUST_PHONE_KEY = DIM_CUST_PHONE_SQL_IN1544.CUST_PHONE_KEY 
 FROM
 DIM_CUST_CNTCT_SQL_IN1544  JOIN DIM_CUST_PHONE_SQL_IN1544  ON
 CONVERT(VARCHAR(50),DIM_CUST_CNTCT_SQL_IN1544.PHONE_ID)=CONVERT(VARCHAR(50),DIM_CUST_PHONE_SQL_IN1544.PHONE_ID)

 ALTER TABLE DIM_CUST_CNTCT_SQL_IN1544 ADD CUST_ADDR_ZONE_KEY INT
 UPDATE DIM_CUST_CNTCT_SQL_IN1544 SET DIM_CUST_CNTCT_SQL_IN1544.CUST_ADDR_ZONE_KEY = dim_cust_addr_zone_SQL_IN1544.ADDR_ZONE_ID_KEY 
 FROM
 DIM_CUST_CNTCT_SQL_IN1544  JOIN dim_cust_addr_zone_SQL_IN1544  ON
 CONVERT(VARCHAR(50),DIM_CUST_CNTCT_SQL_IN1544.ADDR_ZONE_ID )=CONVERT(VARCHAR(50),dim_cust_addr_zone_SQL_IN1544.ADDR_ZONE_ID )

  ALTER TABLE DIM_CUST_CNTCT_SQL_IN1544 ADD CUST_EMAIL_KEY INT
 UPDATE DIM_CUST_CNTCT_SQL_IN1544 SET DIM_CUST_CNTCT_SQL_IN1544.CUST_EMAIL_KEY = DIM_CUST_EMAIL_SQL_IN1544.Cust_email_key 
 FROM
 DIM_CUST_CNTCT_SQL_IN1544  JOIN DIM_CUST_EMAIL_SQL_IN1544  ON
 CONVERT(VARCHAR(50),DIM_CUST_CNTCT_SQL_IN1544.EMAIL_ID )=CONVERT(VARCHAR(50),DIM_CUST_EMAIL_SQL_IN1544.EMAIL_ID )

   ALTER TABLE DIM_CUST_CNTCT_SQL_IN1544 ADD CIUST_ACCNT_KEY INT
 UPDATE DIM_CUST_CNTCT_SQL_IN1544 SET DIM_CUST_CNTCT_SQL_IN1544.CIUST_ACCNT_KEY = dim_CUST_ACCT_SQL_IN1544.cust_acct_key 
 FROM
 DIM_CUST_CNTCT_SQL_IN1544  JOIN dim_CUST_ACCT_SQL_IN1544  ON
 CONVERT(VARCHAR(50),DIM_CUST_CNTCT_SQL_IN1544.ACCT_ID )=CONVERT(VARCHAR(50),dim_CUST_ACCT_SQL_IN1544.ACCT_ID )

 UPDATE DIM_CUST_CNTCT_SQL_IN1544
 set CUST_PHONE_KEY = iif(CUST_PHONE_KEY is null,101,CUST_PHONE_KEY),
	 CUST_ADDR_ZONE_KEY = iif(CUST_ADDR_ZONE_KEY is null,101,CUST_ADDR_ZONE_KEY),
	 CUST_EMAIL_KEY = iif(CUST_EMAIL_KEY is null,101,CUST_EMAIL_KEY),
	 CIUST_ACCNT_KEY = iif(CIUST_ACCNT_KEY is null,101,CIUST_ACCNT_KEY)

 SELECT * FROM DIM_CUST_CNTCT_SQL_IN1544


 select TENANT_ORG_ID,count(*) from
 (select 
iif(CNTCT_ID is null or CNTCT_ID =  'NULL',101,cast(ltrim(rtrim(Cntct_id)) as int)) as CNTCT_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_org_id)) as int)) as TENANT_ORG_ID,
iif(SRC_CNTCT_ID is null or SRC_CNTCT_ID =  'NULL','N/A',ltrim(rtrim(SRC_CNTCT_ID))) as SRC_CNTCT_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(ACCT_ID is null or ACCT_ID =  'NULL',101,cast(ltrim(rtrim(Acct_id)) as int)) as ACCT_ID,
iif(ADDR_ID is null or ADDR_ID =  'NULL',101,cast(ltrim(rtrim(Addr_id)) as bigint)) as ADDR_ID,
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(Phone_id)) as bigint)) as PHONE_ID,
iif(EMAIL_ID is null or EMAIL_ID =  'NULL','N/A',ltrim(rtrim(Email_ID))) as EMAIL_ID,
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID =  'NULL',101,cast(ltrim(rtrim(Addr_Zone_Id))as int)) as ADDR_ZONE_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','Y',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date))  as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL',cast('01-01-1900' as varchar),cast(format(cast(UPD_TS as datetime),'dd/MMMM/yyyy')as varchar)) as UPD_TS
from BCMPWMT.CUST_CNTCT)src
group by TENANT_ORG_ID

select CNTCT_ID, count(*) from DIM_CUST_CNTCT_SQL_IN1544
group by CNTCT_ID
having count(*) > 1

select * from DIM_CUST_CNTCT_SQL_IN1544 t left join 
(select 
iif(CNTCT_ID is null or CNTCT_ID =  'NULL',101,cast(ltrim(rtrim(Cntct_id)) as int)) as CNTCT_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_org_id)) as int)) as TENANT_ORG_ID,
iif(SRC_CNTCT_ID is null or SRC_CNTCT_ID =  'NULL','N/A',ltrim(rtrim(SRC_CNTCT_ID))) as SRC_CNTCT_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(ACCT_ID is null or ACCT_ID =  'NULL',101,cast(ltrim(rtrim(Acct_id)) as int)) as ACCT_ID,
iif(ADDR_ID is null or ADDR_ID =  'NULL',101,cast(ltrim(rtrim(Addr_id)) as bigint)) as ADDR_ID,
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(Phone_id)) as bigint)) as PHONE_ID,
iif(EMAIL_ID is null or EMAIL_ID =  'NULL','N/A',ltrim(rtrim(Email_ID))) as EMAIL_ID,
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID =  'NULL',101,cast(ltrim(rtrim(Addr_Zone_Id))as int)) as ADDR_ZONE_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','Y',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date))  as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL',cast('01-01-1900' as varchar),cast(format(cast(UPD_TS as datetime),'dd/MMMM/yyyy')as varchar)) as UPD_TS
from BCMPWMT.CUST_CNTCT)s
on t.CNTCT_ID = s.CNTCT_ID
where s.CNTCT_ID is null or
(t.CNTCT_ID	<>	s.CNTCT_ID	) or
(t.TENANT_ORG_ID	<>	s.TENANT_ORG_ID	) or
(t.SRC_CNTCT_ID	<>	s.SRC_CNTCT_ID	) or
(t.DATA_SRC_ID	<>	s.DATA_SRC_ID	) or
(t.ACCT_ID	<>	s.ACCT_ID	) or
(t.ADDR_ID	<>	s.ADDR_ID	) or
(t.PHONE_ID	<>	s.PHONE_ID	) or
(t.EMAIL_ID	<>	s.EMAIL_ID	) or
(t.ADDR_ZONE_ID	<>	s.ADDR_ZONE_ID	) or
(t.DELTD_YN	<>	s.DELTD_YN	) or
(t.CRE_DT	<>	s.CRE_DT	) or
(t.UPD_TS	<>	s.UPD_TS	)


select CNTCT_ID,TENANT_ORG_ID,DATA_SRC_ID from
(select 
iif(CNTCT_ID is null or CNTCT_ID =  'NULL',101,cast(ltrim(rtrim(Cntct_id)) as int)) as CNTCT_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_org_id)) as int)) as TENANT_ORG_ID,
iif(SRC_CNTCT_ID is null or SRC_CNTCT_ID =  'NULL','N/A',ltrim(rtrim(SRC_CNTCT_ID))) as SRC_CNTCT_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(ACCT_ID is null or ACCT_ID =  'NULL',101,cast(ltrim(rtrim(Acct_id)) as int)) as ACCT_ID,
iif(ADDR_ID is null or ADDR_ID =  'NULL',101,cast(ltrim(rtrim(Addr_id)) as bigint)) as ADDR_ID,
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(Phone_id)) as bigint)) as PHONE_ID,
iif(EMAIL_ID is null or EMAIL_ID =  'NULL','N/A',ltrim(rtrim(Email_ID))) as EMAIL_ID,
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID =  'NULL',101,cast(ltrim(rtrim(Addr_Zone_Id))as int)) as ADDR_ZONE_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','Y',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date))  as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL',cast('01-01-1900' as varchar),cast(format(cast(UPD_TS as datetime),'dd/MMMM/yyyy')as varchar)) as UPD_TS
from BCMPWMT.CUST_CNTCT)s
where CNTCT_ID = 1382457173

select CNTCT_ID,TENANT_ORG_ID,DATA_SRC_ID from DIM_CUST_CNTCT_SQL_IN1544
where CNTCT_ID = 1382457173

-------------------------------------------------------------------------------------------------------------------------------
select * from dim_RSN_LKP_SQL_IN1544

select * from BCMPWMT.RSN_LKP

insert into dim_RSN_LKP_SQL_IN1544
select 
iif(RSN_ID is null or RSN_ID =  'NULL',101,cast(ltrim(rtrim(rsn_id))as INT)) as RSN_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(tenant_org_id))as INT)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as DATA_SRC_ID,
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as RSN_TYPE_ID,
iif(RSN_CD is null or RSN_CD =  'NULL' or RSN_CD like '%[A-Za-z]%',101,ltrim(rtrim(rsn_cd))) as RSN_CD,
iif(SRC_RSN_ID is null or SRC_RSN_ID =  'NULL',101,cast(ltrim(rtrim(src_rsn_id))as INT)) as SRC_RSN_ID,
iif(RSN_DESC is null or RSN_DESC =  'NULL','N/A',cast(ltrim(rtrim(rsn_desc))as varchar)) as RSN_DESC,
iif(RSN_LONG_DESC is null or RSN_LONG_DESC =  'NULL','N/A',ltrim(rtrim(rsn_long_desc))) as RSN_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(cre_ts))as datetime)) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',CRE_USER) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',UPD_USER) as UPD_USER
from BCMPWMT.RSN_LKP

select TENANT_ORG_ID,count(*) from
(select 
iif(RSN_ID is null or RSN_ID =  'NULL',101,cast(ltrim(rtrim(rsn_id))as INT)) as RSN_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(tenant_org_id))as INT)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as DATA_SRC_ID,
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as RSN_TYPE_ID,
iif(RSN_CD is null or RSN_CD =  'NULL' or RSN_CD like '%[A-Za-z]%',101,ltrim(rtrim(rsn_cd))) as RSN_CD,
iif(SRC_RSN_ID is null or SRC_RSN_ID =  'NULL',101,cast(ltrim(rtrim(src_rsn_id))as INT)) as SRC_RSN_ID,
iif(RSN_DESC is null or RSN_DESC =  'NULL','N/A',cast(ltrim(rtrim(rsn_desc))as varchar)) as RSN_DESC,
iif(RSN_LONG_DESC is null or RSN_LONG_DESC =  'NULL','N/A',ltrim(rtrim(rsn_long_desc))) as RSN_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(cre_ts))as datetime)) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',CRE_USER) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',UPD_USER) as UPD_USER
from BCMPWMT.RSN_LKP)src
group by TENANT_ORG_ID

select TENANT_ORG_ID,count(*) from dim_RSN_LKP_SQL_IN1544
group by TENANT_ORG_ID

select RSN_ID,count(*) from dim_RSN_LKP_SQL_IN1544
group by RSN_ID
having count(*) > 1

select * from dim_RSN_LKP_SQL_IN1544 t left join
(select 
iif(RSN_ID is null or RSN_ID =  'NULL',101,cast(ltrim(rtrim(rsn_id))as INT)) as RSN_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(tenant_org_id))as INT)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as DATA_SRC_ID,
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as RSN_TYPE_ID,
iif(RSN_CD is null or RSN_CD =  'NULL' or RSN_CD like '%[A-Za-z]%',101,ltrim(rtrim(rsn_cd))) as RSN_CD,
iif(SRC_RSN_ID is null or SRC_RSN_ID =  'NULL',101,cast(ltrim(rtrim(src_rsn_id))as INT)) as SRC_RSN_ID,
iif(RSN_DESC is null or RSN_DESC =  'NULL','N/A',cast(ltrim(rtrim(rsn_desc))as varchar)) as RSN_DESC,
iif(RSN_LONG_DESC is null or RSN_LONG_DESC =  'NULL','N/A',ltrim(rtrim(rsn_long_desc))) as RSN_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(cre_ts))as datetime)) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',CRE_USER) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',UPD_USER) as UPD_USER
from BCMPWMT.RSN_LKP)s
on t.RSN_ID = s.RSN_ID
where s.RSN_ID is null or
(t.RSN_ID	<>	s.RSN_ID	) or
(t.TENANT_ORG_ID	<>	s.TENANT_ORG_ID	) or
(t.DATA_SRC_ID	<>	s.DATA_SRC_ID	) or
(t.RSN_TYPE_ID	<>	s.RSN_TYPE_ID	) or
(t.RSN_CD	<>	s.RSN_CD	) or
(t.SRC_RSN_ID	<>	s.SRC_RSN_ID	) or
(t.RSN_DESC	<>	s.RSN_DESC	) or
(t.RSN_LONG_DESC	<>	s.RSN_LONG_DESC	) or
(t.CRE_TS	<>	s.CRE_TS	) or
(t.CRE_USER	<>	s.CRE_USER	) or
(t.UPD_TS	<>	s.UPD_TS	) or
(t.UPD_USER	<>	s.UPD_USER	)

select RSN_ID, TENANT_ORG_ID, SRC_RSN_ID, RSN_LONG_DESC from
(select 
iif(RSN_ID is null or RSN_ID =  'NULL',101,cast(ltrim(rtrim(rsn_id))as INT)) as RSN_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(tenant_org_id))as INT)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as DATA_SRC_ID,
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(data_src_id))as INT)) as RSN_TYPE_ID,
iif(RSN_CD is null or RSN_CD =  'NULL' or RSN_CD like '%[A-Za-z]%',101,ltrim(rtrim(rsn_cd))) as RSN_CD,
iif(SRC_RSN_ID is null or SRC_RSN_ID =  'NULL',101,cast(ltrim(rtrim(src_rsn_id))as INT)) as SRC_RSN_ID,
iif(RSN_DESC is null or RSN_DESC =  'NULL','N/A',cast(ltrim(rtrim(rsn_desc))as varchar)) as RSN_DESC,
iif(RSN_LONG_DESC is null or RSN_LONG_DESC =  'NULL','N/A',ltrim(rtrim(rsn_long_desc))) as RSN_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(cre_ts))as datetime)) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',CRE_USER) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',UPD_USER) as UPD_USER
from BCMPWMT.RSN_LKP)s
where RSN_ID = 281

select RSN_ID, TENANT_ORG_ID, SRC_RSN_ID, RSN_LONG_DESC from
dim_RSN_LKP_SQL_IN1544
where RSN_ID = 281

--------------------------------------------------------------------------------------------------------------------

select * from BCMPWMT.RSN_TYPE_LKP

select * from Dim_RSN_TYPE_LKP_SQL_IN1544

insert into Dim_RSN_TYPE_LKP_SQL_IN1544
select
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(RSN_TYPE_ID)))) as RSN_TYPE_ID,
iif(RSN_TYPE_CD is null or RSN_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_CD))) as RSN_TYPE_CD,
iif(RSN_TYPE_DESC is null or RSN_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_DESC))) as RSN_TYPE_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',CONVERT(DATETIME,LTRIM(RTRIM(CRE_TS)))) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.RSN_TYPE_LKP

select RSN_TYPE_DESC,count(*) from 
(select
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(RSN_TYPE_ID)))) as RSN_TYPE_ID,
iif(RSN_TYPE_CD is null or RSN_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_CD))) as RSN_TYPE_CD,
iif(RSN_TYPE_DESC is null or RSN_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_DESC))) as RSN_TYPE_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',CONVERT(DATETIME,LTRIM(RTRIM(CRE_TS)))) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.RSN_TYPE_LKP)src
group by RSN_TYPE_DESC

select RSN_TYPE_ID,count(*) from Dim_RSN_TYPE_LKP_SQL_IN1544
group by RSN_TYPE_ID
having count(*) > 1


select * from Dim_RSN_TYPE_LKP_SQL_IN1544 t left join
(select
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(RSN_TYPE_ID)))) as RSN_TYPE_ID,
iif(RSN_TYPE_CD is null or RSN_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_CD))) as RSN_TYPE_CD,
iif(RSN_TYPE_DESC is null or RSN_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_DESC))) as RSN_TYPE_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',CONVERT(DATETIME,LTRIM(RTRIM(CRE_TS)))) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.RSN_TYPE_LKP)s
on t.RSN_TYPE_ID = s.RSN_TYPE_ID
where s.RSN_TYPE_ID is null or
(t.RSN_TYPE_ID	<>	s.RSN_TYPE_ID	) or
(t.RSN_TYPE_CD	<>	s.RSN_TYPE_CD	) or
(t.RSN_TYPE_DESC	<>	s.RSN_TYPE_DESC	) or
(t.CRE_TS	<>	s.CRE_TS	) or
(t.CRE_USER	<>	s.CRE_USER	)

select RSN_TYPE_ID, RSN_TYPE_CD, RSN_TYPE_DESC from 
(select
iif(RSN_TYPE_ID is null or RSN_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(RSN_TYPE_ID)))) as RSN_TYPE_ID,
iif(RSN_TYPE_CD is null or RSN_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_CD))) as RSN_TYPE_CD,
iif(RSN_TYPE_DESC is null or RSN_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(RSN_TYPE_DESC))) as RSN_TYPE_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',CONVERT(DATETIME,LTRIM(RTRIM(CRE_TS)))) as CRE_TS,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.RSN_TYPE_LKP)s
where RSN_TYPE_ID = 2

select RSN_TYPE_ID, RSN_TYPE_CD, RSN_TYPE_DESC from
Dim_RSN_TYPE_LKP_SQL_IN1544
where RSN_TYPE_ID = 2


-----------------------------------------------------------------------------------------------------
select * from BCMPWMT.STS_LKP
select * from DIM_STS_LKP_SQL_IN1544

drop table DIM_STS_LKP_SQL_IN1544

create table DIM_STS_LKP_SQL_IN1544
(STS_LKP_KEY	INT	identity(1,1) primary key not null,
STS_ID	INT	not null,
STS_MASTER_ID	INT	not null,
TENANT_ORG_ID	INT	not null,
DATA_SRC_ID	INT	not null,
STS_CD	VARCHAR(50) not null,
SRC_STS_ID	INT	not null,
STS_DESC	VARCHAR(50) not null,
STS_LONG_DESC	VARCHAR(255) not null,
CRE_TS	VARCHAR(50) not null,
UPD_TS	VARCHAR(50) not null)


insert into DIM_STS_LKP_SQL_IN1544
select 
iif(STS_ID is null or STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_ID)))) as STS_ID,
iif(STS_MASTER_ID is null or STS_MASTER_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_MASTER_ID)))) as STS_MASTER_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(TENANT_ORG_ID)))) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(DATA_SRC_ID)))) as DATA_SRC_ID,
iif(STS_CD is null or STS_CD =  'NULL','N/A',CONVERT(VARCHAR,LTRIM(RTRIM(STS_CD)))) as STS_CD,
iif(SRC_STS_ID is null or SRC_STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(SRC_STS_ID)))) as SRC_STS_ID,
iif(STS_DESC is null or STS_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_DESC))) as STS_DESC,
iif(STS_LONG_DESC is null or STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_LONG_DESC))) as STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','N/A',iif(convert(datetime,CRE_TS) = 01-01-2000,'Q1-2000',CRE_TS)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',iif(convert(datetime,UPD_TS) = 01-01-2000,'Q1-2000',UPD_TS)) as UPD_TS
from BCMPWMT.STS_LKP

select 
iif(CRE_TS is null or CRE_TS =  'NULL','N/A',iif(convert(datetime,CRE_TS) = 01-01-2000,'Q1-2000',CRE_TS)) as CRE_TS
from BCMPWMT.STS_LKP

select TENANT_ORG_ID, count(*) from 
(select 
iif(STS_ID is null or STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_ID)))) as STS_ID,
iif(STS_MASTER_ID is null or STS_MASTER_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_MASTER_ID)))) as STS_MASTER_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(TENANT_ORG_ID)))) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(DATA_SRC_ID)))) as DATA_SRC_ID,
iif(STS_CD is null or STS_CD =  'NULL','N/A',CONVERT(VARCHAR,LTRIM(RTRIM(STS_CD)))) as STS_CD,
iif(SRC_STS_ID is null or SRC_STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(SRC_STS_ID)))) as SRC_STS_ID,
iif(STS_DESC is null or STS_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_DESC))) as STS_DESC,
iif(STS_LONG_DESC is null or STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_LONG_DESC))) as STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','N/A',iif(convert(datetime,CRE_TS) = 01-01-2000,'Q1-2000',CRE_TS)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',iif(convert(datetime,UPD_TS) = 01-01-2000,'Q1-2000',UPD_TS)) as UPD_TS
from BCMPWMT.STS_LKP)src
group by TENANT_ORG_ID

select STS_ID, count(*) from DIM_STS_LKP_SQL_IN1544
group by STS_ID
having count(*) > 1

select * from DIM_STS_LKP_SQL_IN1544 t left join
(select 
iif(STS_ID is null or STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_ID)))) as STS_ID,
iif(STS_MASTER_ID is null or STS_MASTER_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_MASTER_ID)))) as STS_MASTER_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(TENANT_ORG_ID)))) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(DATA_SRC_ID)))) as DATA_SRC_ID,
iif(STS_CD is null or STS_CD =  'NULL','N/A',CONVERT(VARCHAR,LTRIM(RTRIM(STS_CD)))) as STS_CD,
iif(SRC_STS_ID is null or SRC_STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(SRC_STS_ID)))) as SRC_STS_ID,
iif(STS_DESC is null or STS_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_DESC))) as STS_DESC,
iif(STS_LONG_DESC is null or STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_LONG_DESC))) as STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','N/A',iif(convert(datetime,CRE_TS) = 01-01-2000,'Q1-2000',CRE_TS)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',iif(convert(datetime,UPD_TS) = 01-01-2000,'Q1-2000',UPD_TS)) as UPD_TS
from BCMPWMT.STS_LKP)s
on t.STS_ID=s.STS_ID
where s.STS_ID is null or
(t.STS_ID	<>	s.STS_ID	) or
(t.STS_MASTER_ID	<>	s.STS_MASTER_ID	) or
(t.TENANT_ORG_ID	<>	s.TENANT_ORG_ID	) or
(t.DATA_SRC_ID	<>	s.DATA_SRC_ID	) or
(t.STS_CD	<>	s.STS_CD	) or
(t.SRC_STS_ID	<>	s.SRC_STS_ID	) or
(t.STS_DESC	<>	s.STS_DESC	) or
(t.STS_LONG_DESC	<>	s.STS_LONG_DESC	) or
(t.CRE_TS	<>	s.CRE_TS	) or
(t.UPD_TS	<>	s.UPD_TS	)

select STS_ID, STS_MASTER_ID, STS_DESC from
(select 
iif(STS_ID is null or STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_ID)))) as STS_ID,
iif(STS_MASTER_ID is null or STS_MASTER_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(STS_MASTER_ID)))) as STS_MASTER_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(TENANT_ORG_ID)))) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(DATA_SRC_ID)))) as DATA_SRC_ID,
iif(STS_CD is null or STS_CD =  'NULL','N/A',CONVERT(VARCHAR,LTRIM(RTRIM(STS_CD)))) as STS_CD,
iif(SRC_STS_ID is null or SRC_STS_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(SRC_STS_ID)))) as SRC_STS_ID,
iif(STS_DESC is null or STS_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_DESC))) as STS_DESC,
iif(STS_LONG_DESC is null or STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(STS_LONG_DESC))) as STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','N/A',iif(convert(datetime,CRE_TS) = 01-01-2000,'Q1-2000',CRE_TS)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',iif(convert(datetime,UPD_TS) = 01-01-2000,'Q1-2000',UPD_TS)) as UPD_TS
from BCMPWMT.STS_LKP)s
where STS_ID = 656

select STS_ID, STS_MASTER_ID, STS_DESC from
DIM_STS_LKP_SQL_IN1544
where STS_ID = 656

