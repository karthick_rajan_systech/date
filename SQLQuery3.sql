insert into DIM_CHARGE_CATEG_SQL_IN1544

select CHARGE_CATEG_ID,TENANT_ORG_ID,CHARGE_CATEG,CHARGE_CATEG_DESC,TAX_IND from
(select 
iif(CHARGE_CATEG_ID is null,101,cast(ltrim(rtrim(CHARGE_CATEG_ID))as int)) as CHARGE_CATEG_ID,
iif(TENANT_ORG_ID is null,101,cast(ltrim(rtrim(TENANT_ORG_ID))as int)) as TENANT_ORG_ID,
iif(CHARGE_CATEG is null,'N/A',iif(len(ltrim(rtrim(CHARGE_CATEG)))>5,ltrim(rtrim(CHARGE_CATEG)),upper(ltrim(rtrim(CHARGE_CATEG))))) as CHARGE_CATEG,
iif(CHARGE_CATEG_DESC is null,'N/A',ltrim(rtrim(charge_categ_desc))) as CHARGE_CATEG_DESC,
iif(TAX_IND is null,101,cast(ltrim(rtrim(TAX_IND))as int)) as TAX_IND,
1 as Version
from BCMPWMT.CHARGE_CATEG_LKP)src 
 where Charge_categ_desc = 'Gift_wrap_tax'

select TENANT_ORG_ID,count(*) from BCMPWMT.CHARGE_CATEG_LKP group by TENANT_ORG_ID

select * from DIM_CHARGE_CATEG_SQL_IN1544 group by CHARGE_CATEG_ID having count(*) > 1


select * from DIM_CHARGE_CATEG_SQL_IN1544 t left join (select CHARGE_CATEG_ID,TENANT_ORG_ID,CHARGE_CATEG,CHARGE_CATEG_DESC,TAX_IND from(select 
iif(CHARGE_CATEG_ID is null,101,cast(ltrim(rtrim(CHARGE_CATEG_ID))as int)) as CHARGE_CATEG_ID,
iif(TENANT_ORG_ID is null,101,cast(ltrim(rtrim(TENANT_ORG_ID))as int)) as TENANT_ORG_ID,
iif(CHARGE_CATEG is null,'N/A',iif(len(ltrim(rtrim(CHARGE_CATEG)))>5,ltrim(rtrim(CHARGE_CATEG)),upper(ltrim(rtrim(CHARGE_CATEG))))) as CHARGE_CATEG,
iif(CHARGE_CATEG_DESC is null,'N/A',ltrim(rtrim(charge_categ_desc))) as CHARGE_CATEG_DESC,
iif(TAX_IND is null,101,cast(ltrim(rtrim(TAX_IND))as int)) as TAX_IND,
1 as Version
from BCMPWMT.CHARGE_CATEG_LKP)src) s 
on t.CHARGE_CATEG_ID=s.CHARGE_CATEG_ID 
where 
s.CHARGE_CATEG_ID is null
or t.TENANT_ORG_ID <> s.TENANT_ORG_ID or t.CHARGE_CATEG <> s.CHARGE_CATEG
or t.CHARGE_CATEG_DESC <> s.CHARGE_CATEG_DESC or t.TAX_IND <> s.TAX_IND

select CHARGE_CATEG_ID,TENANT_ORG_ID,CHARGE_CATEG,CHARGE_CATEG_DESC,TAX_IND from DIM_CHARGE_CATEG_SQL_IN1544 where Charge_categ_desc = 'Gift_wrap_tax'

select CHARGE_CATEG_ID,TENANT_ORG_ID,CHARGE_CATEG,CHARGE_CATEG_DESC,TAX_IND from BCMPWMT.CHARGE_CATEG_LKP where Charge_categ_desc = 'Gift_wrap_tax'


-----------------------------------------------------------------------------------------------------------------------------------------------------------

insert into dim_cust_SQL_IN1544
select 
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id)) as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(TENANT_ORG_ID))as int)) as TENANT_ORG_ID,
iif(CUST_TYPE_ID is null or CUST_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(cust_type_id)) as int)) as CUST_TYPE_ID,
iif(NICKNAME is null or NICKNAME= 'NULL','N/A',concat(upper(substring(ltrim(rtrim(nickname)),1,1)),lower(substring(ltrim(rtrim(nickname)),2,len(ltrim(rtrim(nickname))))))) as NICKNAME,
iif(SALUTE is null or SALUTE= 'NULL','N/A',substring(ltrim(rtrim(salute)),1,4)) as SALUTE,
iif(MIDDLE_NM is null or MIDDLE_NM= 'NULL','N/A',ltrim(rtrim(middle_nm))) as MIDDLE_NM,
iif(CUST_TITLE is null or CUST_TITLE= 'NULL','N/A',replace(ltrim(rtrim(CUST_TITLE)),'#',' ')) as CUST_TITLE,
iif(SUFFIX is null or SUFFIX= 'NULL','N/A',concat(lower(substring(ltrim(rtrim(SUFFIX)),1,1)),upper(substring(ltrim(rtrim(SUFFIX)),2,1)),lower(substring(ltrim(rtrim(SUFFIX)),3,len(ltrim(rtrim(SUFFIX))))))) as SUFFIX,
iif(WM_EMPLOYEE_ID is null or WM_EMPLOYEE_ID= 'NULL',101,cast(ltrim(rtrim(wm_employee_id))as int)) as WM_EMPLOYEE_ID,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as Date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(CRE_user))) as CRE_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
getdate() as Start_date,
null as end_date,
iif(SIGNUP_TS is null or SIGNUP_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(signup_ts))as datetime)) as SIGNUP_TS,
iif(REALM_ID is null or REALM_ID= 'NULL','N/A',ltrim(rtrim(realm_id))) as REALM_ID,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL','N/A',ltrim(rtrim(valid_cust_ind))) as VALID_CUST_IND,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',ltrim(rtrim(deltd_yn))) as DELTD_YN
from BCMPWMT.CUST


iif(Start_Date is null or Start_Date= 'NULL','01-01-1900',
iif(End_Date is null or End_Date= 'NULL','01-01-1900',

select concat(lower(substring(ltrim(rtrim(SUFFIX)),1,1)),upper(substring(ltrim(rtrim(SUFFIX)),2,1)),lower(substring(ltrim(rtrim(SUFFIX)),3,len(ltrim(rtrim(SUFFIX))))))
from BCMPWMT.CUST

select * from BCMPWMT.CUST
select count(*) from dim_cust_SQL_IN1544 group by CUST_ID having count(*) > 1

select * from dim_cust_SQL_IN1544 t left join
(select 
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id)) as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(TENANT_ORG_ID))as int)) as TENANT_ORG_ID,
iif(CUST_TYPE_ID is null or CUST_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(cust_type_id)) as int)) as CUST_TYPE_ID,
iif(NICKNAME is null or NICKNAME= 'NULL','N/A',concat(upper(substring(ltrim(rtrim(nickname)),1,1)),lower(substring(ltrim(rtrim(nickname)),2,len(ltrim(rtrim(nickname))))))) as NICKNAME,
iif(SALUTE is null or SALUTE= 'NULL','N/A',substring(ltrim(rtrim(salute)),1,4)) as SALUTE,
iif(MIDDLE_NM is null or MIDDLE_NM= 'NULL','N/A',ltrim(rtrim(middle_nm))) as MIDDLE_NM,
iif(CUST_TITLE is null or CUST_TITLE= 'NULL','N/A',replace(ltrim(rtrim(CUST_TITLE)),'#',' ')) as CUST_TITLE,
iif(SUFFIX is null or SUFFIX= 'NULL','N/A',concat(lower(substring(ltrim(rtrim(SUFFIX)),1,1)),upper(substring(ltrim(rtrim(SUFFIX)),2,1)),lower(substring(ltrim(rtrim(SUFFIX)),3,len(ltrim(rtrim(SUFFIX))))))) as SUFFIX,
iif(WM_EMPLOYEE_ID is null or WM_EMPLOYEE_ID= 'NULL',101,cast(ltrim(rtrim(wm_employee_id))as int)) as WM_EMPLOYEE_ID,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as Date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(CRE_user))) as CRE_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
getdate() as Start_date,
null as end_date,
iif(SIGNUP_TS is null or SIGNUP_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(signup_ts))as datetime)) as SIGNUP_TS,
iif(REALM_ID is null or REALM_ID= 'NULL','N/A',ltrim(rtrim(realm_id))) as REALM_ID,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL','N/A',ltrim(rtrim(valid_cust_ind))) as VALID_CUST_IND,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',ltrim(rtrim(deltd_yn))) as DELTD_YN
from BCMPWMT.CUST)s
on t.CUST_ID = s.CUST_ID
where s.CUST_ID is null or
t.CUST_ID	<>	s.CUST_ID	or
t.TENANT_ORG_ID	<>	s.TENANT_ORG_ID	or
t.CUST_TYPE_ID	<>	s.CUST_TYPE_ID	or
t.NICKNAME	<>	s.NICKNAME	or
t.SALUTE	<>	s.SALUTE	or
t.MIDDLE_NM	<>	s.MIDDLE_NM	or
t.CUST_TITLE	<>	s.CUST_TITLE	or
t.SUFFIX	<>	s.SUFFIX	or
t.WM_EMPLOYEE_ID	<>	s.WM_EMPLOYEE_ID	or
t.CRE_DT	<>	s.CRE_DT	or
t.CRE_USER	<>	s.CRE_USER	or
t.UPD_TS	<>	s.UPD_TS	or
t.UPD_USER	<>	s.UPD_USER	or
t.SIGNUP_TS	<>	s.SIGNUP_TS	or
t.REALM_ID	<>	s.REALM_ID	or
t.VALID_CUST_IND	<>	s.VALID_CUST_IND	or
t.DELTD_YN	<>	s.DELTD_YN


select cust_id,nickname,salute from dim_cust_SQL_IN1544 where Cust_id=51428720

select cust_id,nickname,salute from 
(select 
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id)) as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(TENANT_ORG_ID))as int)) as TENANT_ORG_ID,
iif(CUST_TYPE_ID is null or CUST_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(cust_type_id)) as int)) as CUST_TYPE_ID,
iif(NICKNAME is null or NICKNAME= 'NULL','N/A',concat(upper(substring(ltrim(rtrim(nickname)),1,1)),lower(substring(ltrim(rtrim(nickname)),2,len(ltrim(rtrim(nickname))))))) as NICKNAME,
iif(SALUTE is null or SALUTE= 'NULL','N/A',substring(ltrim(rtrim(salute)),1,4)) as SALUTE,
iif(MIDDLE_NM is null or MIDDLE_NM= 'NULL','N/A',ltrim(rtrim(middle_nm))) as MIDDLE_NM,
iif(CUST_TITLE is null or CUST_TITLE= 'NULL','N/A',replace(ltrim(rtrim(CUST_TITLE)),'#',' ')) as CUST_TITLE,
iif(SUFFIX is null or SUFFIX= 'NULL','N/A',concat(lower(substring(ltrim(rtrim(SUFFIX)),1,1)),upper(substring(ltrim(rtrim(SUFFIX)),2,1)),lower(substring(ltrim(rtrim(SUFFIX)),3,len(ltrim(rtrim(SUFFIX))))))) as SUFFIX,
iif(WM_EMPLOYEE_ID is null or WM_EMPLOYEE_ID= 'NULL',101,cast(ltrim(rtrim(wm_employee_id))as int)) as WM_EMPLOYEE_ID,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as Date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(CRE_user))) as CRE_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
getdate() as Start_date,
null as end_date,
iif(SIGNUP_TS is null or SIGNUP_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(signup_ts))as datetime)) as SIGNUP_TS,
iif(REALM_ID is null or REALM_ID= 'NULL','N/A',ltrim(rtrim(realm_id))) as REALM_ID,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL','N/A',ltrim(rtrim(valid_cust_ind))) as VALID_CUST_IND,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',ltrim(rtrim(deltd_yn))) as DELTD_YN
from BCMPWMT.CUST)s
where Cust_id=51428720


-----------------------------------------------------------------------------------------------

select * from BCMPWMT.CUST_ACCT
dim_CUST_ACCT_SQL_IN1544

insert into dim_CUST_ACCT_SQL_IN1544
select
iif(ACCT_ID is null or ACCT_ID= 'NULL',101,cast(ltrim(rtrim(acct_id))as bigint)) as ACCT_ID,
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id))as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(ACCT_STS_ID is null or ACCT_STS_ID= 'NULL',101,cast(ltrim(rtrim(acct_sts_id))as int)) as ACCT_STS_ID,
iif(ACCT_TYPE_ID is null or ACCT_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(acct_type_id))as int)) as ACCT_TYPE_ID,
iif(EMAIL is null or EMAIL= 'NULL','N/A',iif(EMAIL like '%@%',ltrim(rtrim(email)),'N/A')) as EMAIL,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL',101,cast(ltrim(rtrim(valid_cust_ind))as int)) as VALID_CUST_IND,
format(iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)),'MM-dd-yyyy') as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
format(iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)),'MM-dd-yyyy HH:mm:ss')as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
format(getdate(),'MM-dd-yyyy HH:mm:ss')as start_date,
null as end_date,
iif(DELTD_YN is null or DELTD_YN= 'NULL','Y','N') as DELTD_YN
from BCMPWMT.CUST_ACCT

select count(*) from dim_CUST_ACCT_SQL_IN1544
select count(*) from dim_cust_SQL_IN1544

select * from BCMPWMT.CUST_ACCT

select CRE_USER,count(*) from
(select
iif(ACCT_ID is null or ACCT_ID= 'NULL',101,cast(ltrim(rtrim(acct_id))as bigint)) as ACCT_ID,
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id))as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(ACCT_STS_ID is null or ACCT_STS_ID= 'NULL',101,cast(ltrim(rtrim(acct_sts_id))as int)) as ACCT_STS_ID,
iif(ACCT_TYPE_ID is null or ACCT_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(acct_type_id))as int)) as ACCT_TYPE_ID,
iif(EMAIL is null or EMAIL= 'NULL','N/A',iif(EMAIL like '%@%',ltrim(rtrim(email)),'N/A')) as EMAIL,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL',101,cast(ltrim(rtrim(valid_cust_ind))as int)) as VALID_CUST_IND,
format(iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)),'MM-dd-yyyy') as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
format(iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)),'MM-dd-yyyy HH:mm:ss')as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
format(getdate(),'MM-dd-yyyy HH:mm:ss')as start_date,
null as end_date,
iif(DELTD_YN is null or DELTD_YN= 'NULL','Y','N') as DELTD_YN
from BCMPWMT.CUST_ACCT)src
group by CRE_USER

select ACCT_ID,count(*) from dim_CUST_ACCT_SQL_IN1544 group by ACCT_ID HAVING count(*) > 1

select * from dim_CUST_ACCT_SQL_IN1544 t left join
(select
iif(ACCT_ID is null or ACCT_ID= 'NULL',101,cast(ltrim(rtrim(acct_id))as bigint)) as ACCT_ID,
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id))as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(ACCT_STS_ID is null or ACCT_STS_ID= 'NULL',101,cast(ltrim(rtrim(acct_sts_id))as int)) as ACCT_STS_ID,
iif(ACCT_TYPE_ID is null or ACCT_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(acct_type_id))as int)) as ACCT_TYPE_ID,
iif(EMAIL is null or EMAIL= 'NULL','N/A',iif(EMAIL like '%@%',ltrim(rtrim(email)),'N/A')) as EMAIL,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL',101,cast(ltrim(rtrim(valid_cust_ind))as int)) as VALID_CUST_IND,
format(iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)),'MM-dd-yyyy') as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
format(iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)),'MM-dd-yyyy HH:mm:ss')as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
format(getdate(),'MM-dd-yyyy HH:mm:ss')as start_date,
null as end_date,
iif(DELTD_YN is null or DELTD_YN= 'NULL','Y','N') as DELTD_YN
from BCMPWMT.CUST_ACCT)s
on t.ACCT_ID = s.ACCT_ID
where s.ACCT_ID is null or
(t.CUST_ID	<>	s.CUST_ID)	or
(t.TENANT_ORG_ID	<>	s.TENANT_ORG_ID)	or
(t.ACCT_STS_ID	<>	s.ACCT_STS_ID)	or
(t.ACCT_TYPE_ID	<>	s.ACCT_TYPE_ID)	or
(t.EMAIL	<>	s.EMAIL)	or
(t.VALID_CUST_IND	<>	s.VALID_CUST_IND)	or
(t.CRE_DT	<>	s.CRE_DT)	or
(t.CRE_USER	<>	s.CRE_USER)	or
(t.UPD_TS	<>	s.UPD_TS)	or
(t.UPD_USER	<>	s.UPD_USER)	or
(t.DELTD_YN	<>	s.DELTD_YN)

select cust_id, cust_id, email from
(select
iif(ACCT_ID is null or ACCT_ID= 'NULL',101,cast(ltrim(rtrim(acct_id))as bigint)) as ACCT_ID,
iif(CUST_ID is null or CUST_ID= 'NULL',101,cast(ltrim(rtrim(cust_id))as int)) as CUST_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(ACCT_STS_ID is null or ACCT_STS_ID= 'NULL',101,cast(ltrim(rtrim(acct_sts_id))as int)) as ACCT_STS_ID,
iif(ACCT_TYPE_ID is null or ACCT_TYPE_ID= 'NULL',101,cast(ltrim(rtrim(acct_type_id))as int)) as ACCT_TYPE_ID,
iif(EMAIL is null or EMAIL= 'NULL','N/A',iif(EMAIL like '%@%',ltrim(rtrim(email)),'N/A')) as EMAIL,
iif(VALID_CUST_IND is null or VALID_CUST_IND= 'NULL',101,cast(ltrim(rtrim(valid_cust_ind))as int)) as VALID_CUST_IND,
format(iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)),'MM-dd-yyyy') as CRE_DT,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
format(iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)),'MM-dd-yyyy HH:mm:ss')as UPD_TS,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
format(getdate(),'MM-dd-yyyy HH:mm:ss')as start_date,
null as end_date,
iif(DELTD_YN is null or DELTD_YN= 'NULL','Y','N') as DELTD_YN
from BCMPWMT.CUST_ACCT)src where acct_id=87005017

select cust_id, cust_id, email from
dim_CUST_ACCT_SQL_IN1544 where acct_id=87005017

-----------------------------------------------------------------------------------------------------------------------------------------
select * from BCMPWMT.CUST_ADDR
select * from dim_cust_address_SQL_IN1544

select
iif(ADDR_ID is null or ADDR_ID= 'NULL',101,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,
iif(DATA_SRC_ID is null or DATA_SRC_ID= 'NULL',101,
iif(VALID_TS is null or VALID_TS= 'NULL','01-01-1900',
iif(VALID_STS is null or VALID_STS= 'NULL','N/A',
iif(CITY is null or CITY= 'NULL','N/A',
iif(MUNICIPALITY is null or MUNICIPALITY= 'NULL','N/A',
iif(TOWN is null or TOWN= 'NULL','N/A',
iif(VILLAGE is null or VILLAGE= 'NULL','N/A',
iif(COUNTY is null or COUNTY= 'NULL','N/A',
iif(DISTRICT is null or DISTRICT= 'NULL','N/A',
iif(ZIP_CD is null or ZIP_CD= 'NULL',101,
iif(POSTAL_CD is null or POSTAL_CD= 'NULL',101,
iif(ZIP_EXTN is null or ZIP_EXTN= 'NULL',101,
iif(ADDR_TYPE is null or ADDR_TYPE= 'NULL','N/A',
iif(AREA is null or AREA= 'NULL','N/A',
iif(CNTRY_CD is null or CNTRY_CD= 'NULL','N/A',
iif(STATE_PRVNCE_TYPE is null or STATE_PRVNCE_TYPE= 'NULL','N/A',
iif(OWNER_ID is null or OWNER_ID= 'NULL',101,
iif(PARENT_ID is null or PARENT_ID= 'NULL',101,
iif(DELTD_YN is null or DELTD_YN= 'NULL',Y,
iif(Start Date is null or Start Date= 'NULL','01-01-1900',
iif(End Date is null or End Date= 'NULL','01-01-1900',
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',
from BCMPWMT.CUST_ADDR


--------------------------------------------------------------------------------------------------------------------------------

select * from BCMPWMT.CUST_ADDR_ZONE
select * from dim_cust_addr_zone_SQL_IN1544

insert into dim_cust_addr_zone_SQL_IN1544
select 
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID= 'NULL',101,cast(ltrim(rtrim(addr_zone_id))as int)) as ADDR_ZONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID= 'NULL',101,cast(ltrim(rtrim(data_src_id))as int)) as DATA_SRC_ID,
iif(CITY is null or CITY= 'NULL','N/A',ltrim(rtrim(city))) as CITY,
iif(POSTAL_CD is null or POSTAL_CD= 'NULL','N/A',ltrim(rtrim(postal_cd))) as POSTAL_CD,
iif(STATE is null or STATE= 'NULL','N/A',ltrim(rtrim(state))) as STATE,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',cast(ltrim(rtrim(deltd_yn))as varchar)) as DELTD_YN,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS
from BCMPWMT.CUST_ADDR_ZONE

select TENANT_ORG_ID,count(*) from
(select 
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID= 'NULL',101,cast(ltrim(rtrim(addr_zone_id))as int)) as ADDR_ZONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID= 'NULL',101,cast(ltrim(rtrim(data_src_id))as int)) as DATA_SRC_ID,
iif(CITY is null or CITY= 'NULL','N/A',ltrim(rtrim(city))) as CITY,
iif(POSTAL_CD is null or POSTAL_CD= 'NULL','N/A',ltrim(rtrim(postal_cd))) as POSTAL_CD,
iif(STATE is null or STATE= 'NULL','N/A',ltrim(rtrim(state))) as STATE,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',cast(ltrim(rtrim(deltd_yn))as varchar)) as DELTD_YN,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS
from BCMPWMT.CUST_ADDR_ZONE)src
group by TENANT_ORG_ID

select ADDR_ZONE_ID,count(*) from dim_cust_addr_zone_SQL_IN1544
group by ADDR_ZONE_ID having count(*) > 1

select * from dim_cust_addr_zone_SQL_IN1544 t left join
(select 
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID= 'NULL',101,cast(ltrim(rtrim(addr_zone_id))as int)) as ADDR_ZONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID= 'NULL',101,cast(ltrim(rtrim(data_src_id))as int)) as DATA_SRC_ID,
iif(CITY is null or CITY= 'NULL','N/A',ltrim(rtrim(city))) as CITY,
iif(POSTAL_CD is null or POSTAL_CD= 'NULL','N/A',ltrim(rtrim(postal_cd))) as POSTAL_CD,
iif(STATE is null or STATE= 'NULL','N/A',ltrim(rtrim(state))) as STATE,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',cast(ltrim(rtrim(deltd_yn))as varchar)) as DELTD_YN,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS
from BCMPWMT.CUST_ADDR_ZONE)s
on t.ADDR_ZONE_ID=s.ADDR_ZONE_ID
where s.ADDR_ZONE_ID is null or
(t.TENANT_ORG_ID<>s.TENANT_ORG_ID) or
(t.DATA_SRC_ID<>s.DATA_SRC_ID) or
(t.CITY<>s.CITY) or
(t.POSTAL_CD<>s.POSTAL_CD) or
(t.STATE<>s.STATE) or
(t.DELTD_YN<>s.DELTD_YN) or
(t.CRE_USER<>s.CRE_USER) or
(t.CRE_DT<>s.CRE_DT) or
(t.UPD_USER<>s.UPD_USER) or
(t.UPD_TS<>s.UPD_TS)


select ADDR_ZONE_ID,CITY from
(select 
iif(ADDR_ZONE_ID is null or ADDR_ZONE_ID= 'NULL',101,cast(ltrim(rtrim(addr_zone_id))as int)) as ADDR_ZONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID= 'NULL',101,cast(ltrim(rtrim(tenant_org_id))as int)) as TENANT_ORG_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID= 'NULL',101,cast(ltrim(rtrim(data_src_id))as int)) as DATA_SRC_ID,
iif(CITY is null or CITY= 'NULL','N/A',ltrim(rtrim(city))) as CITY,
iif(POSTAL_CD is null or POSTAL_CD= 'NULL','N/A',ltrim(rtrim(postal_cd))) as POSTAL_CD,
iif(STATE is null or STATE= 'NULL','N/A',ltrim(rtrim(state))) as STATE,
iif(DELTD_YN is null or DELTD_YN= 'NULL','N/A',cast(ltrim(rtrim(deltd_yn))as varchar)) as DELTD_YN,
iif(CRE_USER is null or CRE_USER= 'NULL','N/A',ltrim(rtrim(cre_user))) as CRE_USER,
iif(CRE_DT is null or CRE_DT= 'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(UPD_USER is null or UPD_USER= 'NULL','N/A',ltrim(rtrim(upd_user))) as UPD_USER,
iif(UPD_TS is null or UPD_TS= 'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS
from BCMPWMT.CUST_ADDR_ZONE)s
where ADDR_ZONE_ID=286104725

select ADDR_ZONE_ID,CITY from dim_cust_addr_zone_SQL_IN1544
where ADDR_ZONE_ID=286104725

-------------------------------------------------------------------------------------------------------------------------
select * from [BCMPWMT].[CUST_ADDR1]

-----------------------------------------------------------------------------------------------------------------------
select * from BCMPWMT.CUST_EMAIL

select * from DIM_CUST_EMAIL_SQL_IN1544

insert into DIM_CUST_EMAIL_SQL_IN1544
select
iif(EMAIL_ID is null or EMAIL_ID =  'NULL',101,cast(ltrim(rtrim(EMAIL_ID)) as bigint)) as EMAIL_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as int)) as CNTCT_TYPE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as varchar) ) as DELTD_YN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Date)),'MM-dd-yyyy') as UPD_TS
from BCMPWMT.CUST_EMAIL


select TENANT_ORG_ID,count(*) from
(select
iif(EMAIL_ID is null or EMAIL_ID =  'NULL',101,cast(ltrim(rtrim(EMAIL_ID)) as bigint)) as EMAIL_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as int)) as CNTCT_TYPE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as varchar) ) as DELTD_YN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Date)),'MM-dd-yyyy') as UPD_TS
from BCMPWMT.CUST_EMAIL)src
group by TENANT_ORG_ID

select TENANT_ORG_ID,count(*) from DIM_CUST_EMAIL_SQL_IN1544
group by TENANT_ORG_ID

select EMAIL_ID,count(*) from DIM_CUST_EMAIL_SQL_IN1544
group by EMAIL_ID having count(*) > 1

select * from DIM_CUST_EMAIL_SQL_IN1544 t left join 
(select
iif(EMAIL_ID is null or EMAIL_ID =  'NULL',101,cast(ltrim(rtrim(EMAIL_ID)) as bigint)) as EMAIL_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as int)) as CNTCT_TYPE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as varchar) ) as DELTD_YN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Date)),'MM-dd-yyyy') as UPD_TS
from BCMPWMT.CUST_EMAIL)s
on t.EMAIL_ID = s.EMAIL_ID
where s.EMAIL_ID is null or
(t.TENANT_ORG_ID	 <> 	s.TENANT_ORG_ID	) or
(t.CNTCT_TYPE_ID	 <> 	s.CNTCT_TYPE_ID	) or
(t.DATA_SRC_ID	 <> 	s.DATA_SRC_ID	) or
(t.DELTD_YN	 <> 	s.DELTD_YN	) or
(t.CRE_DT	 <> 	s.CRE_DT	) or
(t.UPD_TS	 <> 	s.UPD_TS	) 


select EMAIL_ID, DATA_SRC_ID, DELTD_YN from DIM_CUST_EMAIL_SQL_IN1544
where EMAIL_ID = 320514043

select EMAIL_ID, DATA_SRC_ID, DELTD_YN from
(select
iif(EMAIL_ID is null or EMAIL_ID =  'NULL',101,cast(ltrim(rtrim(EMAIL_ID)) as bigint)) as EMAIL_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as int)) as CNTCT_TYPE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as varchar) ) as DELTD_YN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Date)),'MM-dd-yyyy') as UPD_TS
from BCMPWMT.CUST_EMAIL)s
where EMAIL_ID = 320514043

--------------------------------------------------------------------------------------------------------------------------------
select * from BCMPWMT.CUST_PHONE

select * from DIM_CUST_PHONE_SQL_IN1544

insert into DIM_CUST_PHONE_SQL_IN1544
select
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(PHONE_ID)) as bigint)) as PHONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as bigint)) as CNTCT_TYPE_ID,
iif(SRC_PHONE_ID is null or SRC_PHONE_ID =  'NULL','N/A',ltrim(rtrim(Src_phone_id) )) as SRC_PHONE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(AREA_CD is null or AREA_CD =  'NULL','N/A',ltrim(rtrim(AREA_CD))) as AREA_CD,
iif(CNTRY_CD is null or CNTRY_CD =  'NULL','N/A',ltrim(rtrim(CNTRY_CD))) as CNTRY_CD,
iif(EXTN is null or EXTN =  'NULL','N/A',ltrim(rtrim(EXTN))) as EXTN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Datetime)),'MM-dd-yyyy HH:mm:ss') as UPD_TS
from BCMPWMT.CUST_PHONE


select PHONE_ID,count(*) from
(select
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(PHONE_ID)) as bigint)) as PHONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as bigint)) as CNTCT_TYPE_ID,
iif(SRC_PHONE_ID is null or SRC_PHONE_ID =  'NULL','N/A',ltrim(rtrim(Src_phone_id) )) as SRC_PHONE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(AREA_CD is null or AREA_CD =  'NULL','N/A',ltrim(rtrim(AREA_CD))) as AREA_CD,
iif(CNTRY_CD is null or CNTRY_CD =  'NULL','N/A',ltrim(rtrim(CNTRY_CD))) as CNTRY_CD,
iif(EXTN is null or EXTN =  'NULL','N/A',ltrim(rtrim(EXTN))) as EXTN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Datetime)),'MM-dd-yyyy HH:mm:ss') as UPD_TS
from BCMPWMT.CUST_PHONE)src
group by PHONE_ID having count(*) > 1

select PHONE_ID,count(*) from DIM_CUST_PHONE_SQL_IN1544
group by PHONE_ID having count(*) > 1

select * from DIM_CUST_PHONE_SQL_IN1544 t left join
(select
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(PHONE_ID)) as bigint)) as PHONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as bigint)) as CNTCT_TYPE_ID,
iif(SRC_PHONE_ID is null or SRC_PHONE_ID =  'NULL','N/A',ltrim(rtrim(Src_phone_id) )) as SRC_PHONE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(AREA_CD is null or AREA_CD =  'NULL','N/A',ltrim(rtrim(AREA_CD))) as AREA_CD,
iif(CNTRY_CD is null or CNTRY_CD =  'NULL','N/A',ltrim(rtrim(CNTRY_CD))) as CNTRY_CD,
iif(EXTN is null or EXTN =  'NULL','N/A',ltrim(rtrim(EXTN))) as EXTN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Datetime)),'MM-dd-yyyy HH:mm:ss') as UPD_TS
from BCMPWMT.CUST_PHONE)s
on t.PHONE_ID=s.PHONE_ID
where s.PHONE_ID is null or
(t.TENANT_ORG_ID	 <> 	s.TENANT_ORG_ID	) or
(t.CNTCT_TYPE_ID	 <> 	s.CNTCT_TYPE_ID	) or
(t.SRC_PHONE_ID	 <> 	s.SRC_PHONE_ID	) or
(t.DATA_SRC_ID	 <> 	s.DATA_SRC_ID	) or
(t.AREA_CD	 <> 	s.AREA_CD	) or
(t.CNTRY_CD	 <> 	s.CNTRY_CD	) or
(t.EXTN	 <> 	s.EXTN	) or
(t.CRE_DT	 <> 	s.CRE_DT	) or
(t.DELTD_YN	 <> 	s.DELTD_YN	) or
(t.UPD_TS	 <> 	s.UPD_TS)

select PHONE_ID, TENANT_ORG_ID, SRC_PHONE_ID
from 
(select
iif(PHONE_ID is null or PHONE_ID =  'NULL',101,cast(ltrim(rtrim(PHONE_ID)) as bigint)) as PHONE_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL',101,cast(ltrim(rtrim(Tenant_Org_id)) as int)) as TENANT_ORG_ID,
iif(CNTCT_TYPE_ID is null or CNTCT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(cntct_type_id)) as bigint)) as CNTCT_TYPE_ID,
iif(SRC_PHONE_ID is null or SRC_PHONE_ID =  'NULL','N/A',ltrim(rtrim(Src_phone_id) )) as SRC_PHONE_ID,
iif(DATA_SRC_ID is null or DATA_SRC_ID =  'NULL',101,cast(ltrim(rtrim(Data_src_id)) as int)) as DATA_SRC_ID,
iif(AREA_CD is null or AREA_CD =  'NULL','N/A',ltrim(rtrim(AREA_CD))) as AREA_CD,
iif(CNTRY_CD is null or CNTRY_CD =  'NULL','N/A',ltrim(rtrim(CNTRY_CD))) as CNTRY_CD,
iif(EXTN is null or EXTN =  'NULL','N/A',ltrim(rtrim(EXTN))) as EXTN,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(Cre_Dt))as Date)),'MM-dd-yyyy') as CRE_DT,
iif(DELTD_YN is null or DELTD_YN =  'NULL','N/A',cast(ltrim(rtrim(Deltd_yn)) as char))  as DELTD_YN,
format(iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(Upd_ts))as Datetime)),'MM-dd-yyyy HH:mm:ss') as UPD_TS
from BCMPWMT.CUST_PHONE)s
where PHONE_ID = 30271103

select PHONE_ID, TENANT_ORG_ID, SRC_PHONE_ID
from DIM_CUST_PHONE_SQL_IN1544
where PHONE_ID = 30271103

-----------------------------------------------------------------------------------------------------------------------------------------
select * from BCMPWMT.FULFMT_TYPE_LKP
select * from Dim_FULFMT_TYPE_LKP_SQL_IN1544

insert into Dim_FULFMT_TYPE_LKP_SQL_IN1544
select
distinct(iif(FULFMT_TYPE_ID is null or FULFMT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(FULFMT_TYPE_ID)) AS INT))) as FULFMT_TYPE_ID,
iif(FULFMT_TYPE_CD is null or FULFMT_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_CD) )) as FULFMT_TYPE_CD,
iif(FULFMT_TYPE_DESC is null or FULFMT_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_DESC) )) as FULFMT_TYPE_DESC,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(CRE_DT ))AS DATE)),'MM-dd-yyyy') as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',format(cast(UPD_TS as date),'ddMMyyyy')) as UPD_TS
from BCMPWMT.FULFMT_TYPE_LKP


select FULFMT_TYPE_DESC,count(*) from 
(select
distinct(iif(FULFMT_TYPE_ID is null or FULFMT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(FULFMT_TYPE_ID)) AS INT))) as FULFMT_TYPE_ID,
iif(FULFMT_TYPE_CD is null or FULFMT_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_CD) )) as FULFMT_TYPE_CD,
iif(FULFMT_TYPE_DESC is null or FULFMT_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_DESC) )) as FULFMT_TYPE_DESC,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(CRE_DT ))AS DATE)),'MM-dd-yyyy') as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',format(cast(UPD_TS as date),'ddMMyyyy')) as UPD_TS
from BCMPWMT.FULFMT_TYPE_LKP)src
group by FULFMT_TYPE_DESC

select FULFMT_TYPE_DESC,count(*) from Dim_FULFMT_TYPE_LKP_SQL_IN1544
group by FULFMT_TYPE_DESC 
having count(*) > 1

select * from Dim_FULFMT_TYPE_LKP_SQL_IN1544 t left join 
(select
distinct(iif(FULFMT_TYPE_ID is null or FULFMT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(FULFMT_TYPE_ID)) AS INT))) as FULFMT_TYPE_ID,
iif(FULFMT_TYPE_CD is null or FULFMT_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_CD) )) as FULFMT_TYPE_CD,
iif(FULFMT_TYPE_DESC is null or FULFMT_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_DESC) )) as FULFMT_TYPE_DESC,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(CRE_DT ))AS DATE)),'MM-dd-yyyy') as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',format(cast(UPD_TS as date),'ddMMyyyy')) as UPD_TS
from BCMPWMT.FULFMT_TYPE_LKP)s
on t.FULFMT_TYPE_ID = s.FULFMT_TYPE_ID
where s.FULFMT_TYPE_ID is null or 
(t.FULFMT_TYPE_CD	 <> 	s.FULFMT_TYPE_CD	) or
(t.FULFMT_TYPE_DESC	 <> 	s.FULFMT_TYPE_DESC	) or
(t.CRE_DT	 <> 	s.CRE_DT	) or
(t.UPD_TS	 <> 	s.UPD_TS	) 


select FULFMT_TYPE_ID, FULFMT_TYPE_CD, FULFMT_TYPE_DESC,CRE_DT from
Dim_FULFMT_TYPE_LKP_SQL_IN1544
where FULFMT_TYPE_ID = 1


select FULFMT_TYPE_ID, FULFMT_TYPE_CD, FULFMT_TYPE_DESC,CRE_DT from
(select
distinct(iif(FULFMT_TYPE_ID is null or FULFMT_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(FULFMT_TYPE_ID)) AS INT))) as FULFMT_TYPE_ID,
iif(FULFMT_TYPE_CD is null or FULFMT_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_CD) )) as FULFMT_TYPE_CD,
iif(FULFMT_TYPE_DESC is null or FULFMT_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(FULFMT_TYPE_DESC) )) as FULFMT_TYPE_DESC,
format(iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(CRE_DT ))AS DATE)),'MM-dd-yyyy') as CRE_DT,
iif(UPD_TS is null or UPD_TS =  'NULL','N/A',format(cast(UPD_TS as date),'ddMMyyyy')) as UPD_TS
from BCMPWMT.FULFMT_TYPE_LKP)s
where FULFMT_TYPE_ID = 1

----------------------------------------------------------------------------------------------------------------------------
create table dim_prod_SQL_IN1544
(prod_key int identity(1,1) primary key not null,
CATLG_ITEM_ID int not null,
PRMRY_DSTRBTR_NM varchar(200) not null,
PRMRY_VEND_NUM int not null,
SRC_IMS_CRE_TS varchar(50) not null,
SRC_IMS_MODFD_TS varchar(50) not null,
VEND_PACK_QTY int not null,
WHSE_PACK_QTY int not null,
CURR_PRICE_MODFD_TS datetime not null,
AMT_ITEM_COST decimal(19,6) not null,
AMT_BASE_ITEM_PRICE decimal(19,6) not null,
AMT_BASE_SUGG_PRICE decimal(19,6) not null,
AMT_SUGG_PRICE decimal(19,6) not null,
MIN_ITEM_COST decimal(19,6) not null,
ORIG_PRICE decimal(19,6) not null,
ORIG_ITEM_PRICE decimal(19,6) not null,
PROD_NM varchar(200) not null,
PROD_HT decimal(19,6) not null,
PROD_WT decimal(19,6) not null,
PROD_LEN decimal(19,6) not null,
PROD_WDTH decimal(19,6) not null,
CRE_DT date not null,
CRE_USER varchar(50) not null,
UPD_TS datetime not null,
UPD_USER varchar(50) not null)


select * from BCMPWMT.PROD

select * from dim_prod_SQL_IN1544

insert into dim_prod_SQL_IN1544
select
iif(CATLG_ITEM_ID is null or CATLG_ITEM_ID =  'NULL',101,cast(ltrim(rtrim(CATLG_ITEM_ID)) as int)) as CATLG_ITEM_ID,
iif(PRMRY_DSTRBTR_NM is null or PRMRY_DSTRBTR_NM =  'NULL','N/A',ltrim(rtrim(PRMRY_DSTRBTR_NM))) as PRMRY_DSTRBTR_NM,
iif(PRMRY_VEND_NUM is null or PRMRY_VEND_NUM =  'NULL',101,cast(ltrim(rtrim(PRMRY_VEND_NUM)) as int)) as PRMRY_VEND_NUM,
iif(SRC_IMS_CRE_TS is null or SRC_IMS_CRE_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_CRE_TS) as SRC_IMS_CRE_TS,
iif(SRC_IMS_MODFD_TS is null or SRC_IMS_MODFD_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_MODFD_TS) as SRC_IMS_MODFD_TS,
iif(VEND_PACK_QTY is null or VEND_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(VEND_PACK_QTY)) as int)) as VEND_PACK_QTY,
iif(WHSE_PACK_QTY is null or WHSE_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(WHSE_PACK_QTY)) as int)) as WHSE_PACK_QTY,
iif(CURR_PRICE_MODFD_TS is null or CURR_PRICE_MODFD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(CURR_PRICE_MODFD_TS))as datetime)) as CURR_PRICE_MODFD_TS,
iif(AMT_ITEM_COST is null or AMT_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(AMT_ITEM_COST))as decimal)) as AMT_ITEM_COST,
iif(AMT_BASE_ITEM_PRICE is null or AMT_BASE_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_ITEM_PRICE))as decimal)) as AMT_BASE_ITEM_PRICE,
iif(AMT_BASE_SUGG_PRICE is null or AMT_BASE_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_SUGG_PRICE))as decimal)) as AMT_BASE_SUGG_PRICE,
iif(AMT_SUGG_PRICE is null or AMT_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_SUGG_PRICE))as decimal)) as AMT_SUGG_PRICE,
iif(MIN_ITEM_COST is null or MIN_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(MIN_ITEM_COST))as decimal)) as MIN_ITEM_COST,
iif(ORIG_PRICE is null or ORIG_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_PRICE))as decimal)) as ORIG_PRICE,
iif(ORIG_ITEM_PRICE is null or ORIG_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_ITEM_PRICE))as decimal)) as ORIG_ITEM_PRICE,
iif(PROD_NM is null or PROD_NM =  'NULL','N/A',LTRIM(RTRIM(PROD_NM))) as PROD_NM,
iif(PROD_HT is null or PROD_HT =  'NULL' or PROD_HT like '%[A-Z0-9]%',101,cast(ltrim(rtrim(prod_ht))as decimal)) as PROD_HT,
iif(PROD_WT is null or PROD_WT =  'NULL',101,cast(ltrim(rtrim(prod_wt))as decimal)) as PROD_WT,
iif(PROD_LEN is null or PROD_LEN =  'NULL',101,cast(ltrim(rtrim(prod_len))as decimal)) as PROD_LEN,
iif(PROD_WDTH is null or PROD_WDTH =  'NULL',101,cast(ltrim(rtrim(prod_wdth))as decimal)) as PROD_WDTH,
iif(CRE_DT is null or CRE_DT =  'NULL' or CRE_DT like '%[A-Z]%','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',ltrim(rtrim(CRE_USER))) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL' or UPD_TS like '%[A-Z0-9]%','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',ltrim(rtrim(UPD_USER))) as UPD_USER
from BCMPWMT.PROD


select CRE_USER,count(*) from
(select
iif(CATLG_ITEM_ID is null or CATLG_ITEM_ID =  'NULL',101,cast(ltrim(rtrim(CATLG_ITEM_ID)) as int)) as CATLG_ITEM_ID,
iif(PRMRY_DSTRBTR_NM is null or PRMRY_DSTRBTR_NM =  'NULL','N/A',ltrim(rtrim(PRMRY_DSTRBTR_NM))) as PRMRY_DSTRBTR_NM,
iif(PRMRY_VEND_NUM is null or PRMRY_VEND_NUM =  'NULL',101,cast(ltrim(rtrim(PRMRY_VEND_NUM)) as int)) as PRMRY_VEND_NUM,
iif(SRC_IMS_CRE_TS is null or SRC_IMS_CRE_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_CRE_TS) as SRC_IMS_CRE_TS,
iif(SRC_IMS_MODFD_TS is null or SRC_IMS_MODFD_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_MODFD_TS) as SRC_IMS_MODFD_TS,
iif(VEND_PACK_QTY is null or VEND_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(VEND_PACK_QTY)) as int)) as VEND_PACK_QTY,
iif(WHSE_PACK_QTY is null or WHSE_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(WHSE_PACK_QTY)) as int)) as WHSE_PACK_QTY,
iif(CURR_PRICE_MODFD_TS is null or CURR_PRICE_MODFD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(CURR_PRICE_MODFD_TS))as datetime)) as CURR_PRICE_MODFD_TS,
iif(AMT_ITEM_COST is null or AMT_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(AMT_ITEM_COST))as decimal)) as AMT_ITEM_COST,
iif(AMT_BASE_ITEM_PRICE is null or AMT_BASE_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_ITEM_PRICE))as decimal)) as AMT_BASE_ITEM_PRICE,
iif(AMT_BASE_SUGG_PRICE is null or AMT_BASE_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_SUGG_PRICE))as decimal)) as AMT_BASE_SUGG_PRICE,
iif(AMT_SUGG_PRICE is null or AMT_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_SUGG_PRICE))as decimal)) as AMT_SUGG_PRICE,
iif(MIN_ITEM_COST is null or MIN_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(MIN_ITEM_COST))as decimal)) as MIN_ITEM_COST,
iif(ORIG_PRICE is null or ORIG_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_PRICE))as decimal)) as ORIG_PRICE,
iif(ORIG_ITEM_PRICE is null or ORIG_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_ITEM_PRICE))as decimal)) as ORIG_ITEM_PRICE,
iif(PROD_NM is null or PROD_NM =  'NULL','N/A',LTRIM(RTRIM(PROD_NM))) as PROD_NM,
iif(PROD_HT is null or PROD_HT =  'NULL' or PROD_HT like '%[A-Z0-9]%',101,cast(ltrim(rtrim(prod_ht))as decimal)) as PROD_HT,
iif(PROD_WT is null or PROD_WT =  'NULL',101,cast(ltrim(rtrim(prod_wt))as decimal)) as PROD_WT,
iif(PROD_LEN is null or PROD_LEN =  'NULL',101,cast(ltrim(rtrim(prod_len))as decimal)) as PROD_LEN,
iif(PROD_WDTH is null or PROD_WDTH =  'NULL',101,cast(ltrim(rtrim(prod_wdth))as decimal)) as PROD_WDTH,
iif(CRE_DT is null or CRE_DT =  'NULL' or CRE_DT like '%[A-Z]%','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',ltrim(rtrim(CRE_USER))) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL' or UPD_TS like '%[A-Z0-9]%','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',ltrim(rtrim(UPD_USER))) as UPD_USER
from BCMPWMT.PROD)src
group by CRE_USER

select CATLG_ITEM_ID,count(*) from dim_prod_SQL_IN1544
group by CATLG_ITEM_ID
having count(*) > 1

select * from dim_prod_SQL_IN1544 t left join
(select
iif(CATLG_ITEM_ID is null or CATLG_ITEM_ID =  'NULL',101,cast(ltrim(rtrim(CATLG_ITEM_ID)) as int)) as CATLG_ITEM_ID,
iif(PRMRY_DSTRBTR_NM is null or PRMRY_DSTRBTR_NM =  'NULL','N/A',ltrim(rtrim(PRMRY_DSTRBTR_NM))) as PRMRY_DSTRBTR_NM,
iif(PRMRY_VEND_NUM is null or PRMRY_VEND_NUM =  'NULL',101,cast(ltrim(rtrim(PRMRY_VEND_NUM)) as int)) as PRMRY_VEND_NUM,
iif(SRC_IMS_CRE_TS is null or SRC_IMS_CRE_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_CRE_TS) as SRC_IMS_CRE_TS,
iif(SRC_IMS_MODFD_TS is null or SRC_IMS_MODFD_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_MODFD_TS) as SRC_IMS_MODFD_TS,
iif(VEND_PACK_QTY is null or VEND_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(VEND_PACK_QTY)) as int)) as VEND_PACK_QTY,
iif(WHSE_PACK_QTY is null or WHSE_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(WHSE_PACK_QTY)) as int)) as WHSE_PACK_QTY,
iif(CURR_PRICE_MODFD_TS is null or CURR_PRICE_MODFD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(CURR_PRICE_MODFD_TS))as datetime)) as CURR_PRICE_MODFD_TS,
iif(AMT_ITEM_COST is null or AMT_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(AMT_ITEM_COST))as decimal)) as AMT_ITEM_COST,
iif(AMT_BASE_ITEM_PRICE is null or AMT_BASE_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_ITEM_PRICE))as decimal)) as AMT_BASE_ITEM_PRICE,
iif(AMT_BASE_SUGG_PRICE is null or AMT_BASE_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_SUGG_PRICE))as decimal)) as AMT_BASE_SUGG_PRICE,
iif(AMT_SUGG_PRICE is null or AMT_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_SUGG_PRICE))as decimal)) as AMT_SUGG_PRICE,
iif(MIN_ITEM_COST is null or MIN_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(MIN_ITEM_COST))as decimal)) as MIN_ITEM_COST,
iif(ORIG_PRICE is null or ORIG_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_PRICE))as decimal)) as ORIG_PRICE,
iif(ORIG_ITEM_PRICE is null or ORIG_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_ITEM_PRICE))as decimal)) as ORIG_ITEM_PRICE,
iif(PROD_NM is null or PROD_NM =  'NULL','N/A',LTRIM(RTRIM(PROD_NM))) as PROD_NM,
iif(PROD_HT is null or PROD_HT =  'NULL' or PROD_HT like '%[A-Z0-9]%',101,cast(ltrim(rtrim(prod_ht))as decimal)) as PROD_HT,
iif(PROD_WT is null or PROD_WT =  'NULL',101,cast(ltrim(rtrim(prod_wt))as decimal)) as PROD_WT,
iif(PROD_LEN is null or PROD_LEN =  'NULL',101,cast(ltrim(rtrim(prod_len))as decimal)) as PROD_LEN,
iif(PROD_WDTH is null or PROD_WDTH =  'NULL',101,cast(ltrim(rtrim(prod_wdth))as decimal)) as PROD_WDTH,
iif(CRE_DT is null or CRE_DT =  'NULL' or CRE_DT like '%[A-Z]%','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',ltrim(rtrim(CRE_USER))) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL' or UPD_TS like '%[A-Z0-9]%','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',ltrim(rtrim(UPD_USER))) as UPD_USER
from BCMPWMT.PROD)s
on t.CATLG_ITEM_ID = s.CATLG_ITEM_ID
where s.CATLG_ITEM_ID is null or
(t.PRMRY_DSTRBTR_NM	 <> 	s.PRMRY_DSTRBTR_NM	) or
(t.PRMRY_VEND_NUM	 <> 	s.PRMRY_VEND_NUM	) or
(t.SRC_IMS_CRE_TS	 <> 	s.SRC_IMS_CRE_TS	) or
(t.SRC_IMS_MODFD_TS	 <> 	s.SRC_IMS_MODFD_TS	) or
(t.VEND_PACK_QTY	 <> 	s.VEND_PACK_QTY	) or
(t.WHSE_PACK_QTY	 <> 	s.WHSE_PACK_QTY	) or
(t.CURR_PRICE_MODFD_TS	 <> 	s.CURR_PRICE_MODFD_TS	) or
(t.AMT_ITEM_COST	 <> 	s.AMT_ITEM_COST	) or
(t.AMT_BASE_ITEM_PRICE	 <> 	s.AMT_BASE_ITEM_PRICE	) or
(t.AMT_BASE_SUGG_PRICE	 <> 	s.AMT_BASE_SUGG_PRICE	) or
(t.AMT_SUGG_PRICE	 <> 	s.AMT_SUGG_PRICE	) or
(t.MIN_ITEM_COST	 <> 	s.MIN_ITEM_COST	) or
(t.ORIG_PRICE	 <> 	s.ORIG_PRICE	) or
(t.ORIG_ITEM_PRICE	 <> 	s.ORIG_ITEM_PRICE	) or
(t.PROD_NM	 <> 	s.PROD_NM	) or
(t.PROD_HT	 <> 	s.PROD_HT	) or
(t.PROD_WT	 <> 	s.PROD_WT	) or
(t.PROD_LEN	 <> 	s.PROD_LEN	) or
(t.PROD_WDTH	 <> 	s.PROD_WDTH	) or
(t.CRE_DT	 <> 	s.CRE_DT	) or
(t.CRE_USER	 <> 	s.CRE_USER	) or
(t.UPD_TS	 <> 	s.UPD_TS	) or
(t.UPD_USER	 <> 	s.UPD_USER	)

select CATLG_ITEM_ID, PRMRY_DSTRBTR_NM, ORIG_ITEM_PRICE from
(select
iif(CATLG_ITEM_ID is null or CATLG_ITEM_ID =  'NULL',101,cast(ltrim(rtrim(CATLG_ITEM_ID)) as int)) as CATLG_ITEM_ID,
iif(PRMRY_DSTRBTR_NM is null or PRMRY_DSTRBTR_NM =  'NULL','N/A',ltrim(rtrim(PRMRY_DSTRBTR_NM))) as PRMRY_DSTRBTR_NM,
iif(PRMRY_VEND_NUM is null or PRMRY_VEND_NUM =  'NULL',101,cast(ltrim(rtrim(PRMRY_VEND_NUM)) as int)) as PRMRY_VEND_NUM,
iif(SRC_IMS_CRE_TS is null or SRC_IMS_CRE_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_CRE_TS) as SRC_IMS_CRE_TS,
iif(SRC_IMS_MODFD_TS is null or SRC_IMS_MODFD_TS =  'NULL','01-01-1900 00:00:00',SRC_IMS_MODFD_TS) as SRC_IMS_MODFD_TS,
iif(VEND_PACK_QTY is null or VEND_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(VEND_PACK_QTY)) as int)) as VEND_PACK_QTY,
iif(WHSE_PACK_QTY is null or WHSE_PACK_QTY =  'NULL',101,cast(ltrim(rtrim(WHSE_PACK_QTY)) as int)) as WHSE_PACK_QTY,
iif(CURR_PRICE_MODFD_TS is null or CURR_PRICE_MODFD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(CURR_PRICE_MODFD_TS))as datetime)) as CURR_PRICE_MODFD_TS,
iif(AMT_ITEM_COST is null or AMT_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(AMT_ITEM_COST))as decimal)) as AMT_ITEM_COST,
iif(AMT_BASE_ITEM_PRICE is null or AMT_BASE_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_ITEM_PRICE))as decimal)) as AMT_BASE_ITEM_PRICE,
iif(AMT_BASE_SUGG_PRICE is null or AMT_BASE_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_BASE_SUGG_PRICE))as decimal)) as AMT_BASE_SUGG_PRICE,
iif(AMT_SUGG_PRICE is null or AMT_SUGG_PRICE =  'NULL',101,cast(ltrim(rtrim(AMT_SUGG_PRICE))as decimal)) as AMT_SUGG_PRICE,
iif(MIN_ITEM_COST is null or MIN_ITEM_COST =  'NULL',101,cast(ltrim(rtrim(MIN_ITEM_COST))as decimal)) as MIN_ITEM_COST,
iif(ORIG_PRICE is null or ORIG_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_PRICE))as decimal)) as ORIG_PRICE,
iif(ORIG_ITEM_PRICE is null or ORIG_ITEM_PRICE =  'NULL',101,cast(ltrim(rtrim(ORIG_ITEM_PRICE))as decimal)) as ORIG_ITEM_PRICE,
iif(PROD_NM is null or PROD_NM =  'NULL','N/A',LTRIM(RTRIM(PROD_NM))) as PROD_NM,
iif(PROD_HT is null or PROD_HT =  'NULL' or PROD_HT like '%[A-Z0-9]%',101,cast(ltrim(rtrim(prod_ht))as decimal)) as PROD_HT,
iif(PROD_WT is null or PROD_WT =  'NULL',101,cast(ltrim(rtrim(prod_wt))as decimal)) as PROD_WT,
iif(PROD_LEN is null or PROD_LEN =  'NULL',101,cast(ltrim(rtrim(prod_len))as decimal)) as PROD_LEN,
iif(PROD_WDTH is null or PROD_WDTH =  'NULL',101,cast(ltrim(rtrim(prod_wdth))as decimal)) as PROD_WDTH,
iif(CRE_DT is null or CRE_DT =  'NULL' or CRE_DT like '%[A-Z]%','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',ltrim(rtrim(CRE_USER))) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL' or UPD_TS like '%[A-Z0-9]%','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',ltrim(rtrim(UPD_USER))) as UPD_USER
from BCMPWMT.PROD)s
where CATLG_ITEM_ID = 37348546

select CATLG_ITEM_ID, PRMRY_DSTRBTR_NM, ORIG_ITEM_PRICE from
dim_prod_SQL_IN1544
where CATLG_ITEM_ID = 37348546

select count(*) from dim_prod_SQL_IN1544

select count(*) from IN1544.dim_prod_SQL_IN1544

------------------------------------------------------------------------------------------------------------------------------------
create table Dim_ORDER_STS_MASTER_LKP_SQL_IN1544
(ORDER_STS_MASTER_ID bigint primary key not null,	
ORDER_STS_MASTER_CD VARCHAR(50) not null,
ORDER_STS_SHORT_DESC VARCHAR(50) not null,
ORDER_STS_LONG_DESC VARCHAR(50) not null,
CRE_TS DATETIME	not null,
UPD_TS DATETIME	not null,
ORDER_STS_MASTER_LKP_KEY INT identity(1,1) not null)




select distinct(ORDER_STS_MASTER_CD) from BCMPWMT.ORDER_STS_MASTER_LKP
select * from Dim_ORDER_STS_MASTER_LKP_SQL_IN1544

select * from BCMPWMT.ORDER_STS_MASTER_LKP
select distinct(ORDER_STS_MASTER_ID) from BCMPWMT.ORDER_STS_MASTER_LKP

insert into Dim_ORDER_STS_MASTER_LKP_SQL_IN1544
select
iif(ORDER_STS_MASTER_ID is null ,101,cast(ltrim(rtrim(ORDER_STS_MASTER_ID)) as bigint)) as ORDER_STS_MASTER_ID,
iif(ORDER_STS_MASTER_CD is null or ORDER_STS_MASTER_CD =  'NULL' ,'N/A',LTRIM(RTRIM(ORDER_STS_MASTER_CD))) as ORDER_STS_MASTER_CD,
iif(ORDER_STS_SHORT_DESC is null or ORDER_STS_SHORT_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_SHORT_DESC))) as ORDER_STS_SHORT_DESC,
iif(ORDER_STS_LONG_DESC is null or ORDER_STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_LONG_DESC))) as ORDER_STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(CRE_TS,1,charindex(' P',CRE_TS))))AS DATETIME)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(UPD_TS,1,charindex(' P',UPD_TS))))AS DATETIME)) as UPD_TS
from BCMPWMT.ORDER_STS_MASTER_LKP

select
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',ltrim(rtrim(substring(CRE_TS,1,charindex(' P',CRE_TS))))) as CRE_TS
from BCMPWMT.ORDER_STS_MASTER_LKP

select CRE_TS,count(*) from
(select
iif(ORDER_STS_MASTER_ID is null ,101,cast(ltrim(rtrim(ORDER_STS_MASTER_ID)) as bigint)) as ORDER_STS_MASTER_ID,
iif(ORDER_STS_MASTER_CD is null or ORDER_STS_MASTER_CD =  'NULL' ,'N/A',LTRIM(RTRIM(ORDER_STS_MASTER_CD))) as ORDER_STS_MASTER_CD,
iif(ORDER_STS_SHORT_DESC is null or ORDER_STS_SHORT_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_SHORT_DESC))) as ORDER_STS_SHORT_DESC,
iif(ORDER_STS_LONG_DESC is null or ORDER_STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_LONG_DESC))) as ORDER_STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(CRE_TS,1,charindex(' P',CRE_TS))))AS DATETIME)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(UPD_TS,1,charindex(' P',UPD_TS))))AS DATETIME)) as UPD_TS
from BCMPWMT.ORDER_STS_MASTER_LKP)src
group by CRE_TS

select ORDER_STS_MASTER_ID,count(*) from Dim_ORDER_STS_MASTER_LKP_SQL_IN1544
group by ORDER_STS_MASTER_ID
having count(*) > 1

select * from Dim_ORDER_STS_MASTER_LKP_SQL_IN1544 t left join
(select
iif(ORDER_STS_MASTER_ID is null ,101,cast(ltrim(rtrim(ORDER_STS_MASTER_ID)) as bigint)) as ORDER_STS_MASTER_ID,
iif(ORDER_STS_MASTER_CD is null or ORDER_STS_MASTER_CD =  'NULL' ,'N/A',LTRIM(RTRIM(ORDER_STS_MASTER_CD))) as ORDER_STS_MASTER_CD,
iif(ORDER_STS_SHORT_DESC is null or ORDER_STS_SHORT_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_SHORT_DESC))) as ORDER_STS_SHORT_DESC,
iif(ORDER_STS_LONG_DESC is null or ORDER_STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_LONG_DESC))) as ORDER_STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(CRE_TS,1,charindex(' P',CRE_TS))))AS DATETIME)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(UPD_TS,1,charindex(' P',UPD_TS))))AS DATETIME)) as UPD_TS
from BCMPWMT.ORDER_STS_MASTER_LKP)s
on t.ORDER_STS_MASTER_ID = s.ORDER_STS_MASTER_ID
where s.ORDER_STS_MASTER_ID is null or
(t.ORDER_STS_MASTER_ID	<>	s.ORDER_STS_MASTER_ID	) or
(t.ORDER_STS_MASTER_CD	<>	s.ORDER_STS_MASTER_CD	) or
(t.ORDER_STS_SHORT_DESC	<>	s.ORDER_STS_SHORT_DESC	) or
(t.ORDER_STS_LONG_DESC	<>	s.ORDER_STS_LONG_DESC	) or
(t.CRE_TS	<>	s.CRE_TS	) or
(t.UPD_TS	<>	s.UPD_TS	)


select ORDER_STS_MASTER_ID,ORDER_STS_MASTER_CD,ORDER_STS_SHORT_DESC from
(select
iif(ORDER_STS_MASTER_ID is null ,101,cast(ltrim(rtrim(ORDER_STS_MASTER_ID)) as bigint)) as ORDER_STS_MASTER_ID,
iif(ORDER_STS_MASTER_CD is null or ORDER_STS_MASTER_CD =  'NULL' ,'N/A',LTRIM(RTRIM(ORDER_STS_MASTER_CD))) as ORDER_STS_MASTER_CD,
iif(ORDER_STS_SHORT_DESC is null or ORDER_STS_SHORT_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_SHORT_DESC))) as ORDER_STS_SHORT_DESC,
iif(ORDER_STS_LONG_DESC is null or ORDER_STS_LONG_DESC =  'NULL','N/A',LTRIM(RTRIM(ORDER_STS_LONG_DESC))) as ORDER_STS_LONG_DESC,
iif(CRE_TS is null or CRE_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(CRE_TS,1,charindex(' P',CRE_TS))))AS DATETIME)) as CRE_TS,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(substring(UPD_TS,1,charindex(' P',UPD_TS))))AS DATETIME)) as UPD_TS
from BCMPWMT.ORDER_STS_MASTER_LKP)src
where ORDER_STS_MASTER_ID = 35

select ORDER_STS_MASTER_ID,ORDER_STS_MASTER_CD,ORDER_STS_SHORT_DESC from
Dim_ORDER_STS_MASTER_LKP_SQL_IN1544
where ORDER_STS_MASTER_ID = 35



-------------------------------------------------------------------------------------------------------------------------------------
select * from BCMPWMT.ORG_BUSINESS_UNIT

select * from DIM_ORG_BUSINESS_UNIT_SQL_IN1544

drop table DIM_ORG_BUSINESS_UNIT_SQL_IN1544

create table DIM_ORG_BUSINESS_UNIT_SQL_IN1544
(ORG_ID varchar(50) not null,
SRC_ORG_CD varchar(50) not null,
ORG_TYPE_ID INT	not null,
ORG_NM varchar(250) not null,
PARENT_ORG_ID varchar(50) not null,
PARENT_ORG_NM varchar(50) not null,
WM_RDC_NUM varchar(50) not null,
WM_STORE_NUM varchar(50) not null,
WM_DSTRBTR_NO varchar(50) not null,
WH_IND bigint not null,
DSV_IND bigint not null,
ACTV_IND bigint not null,
EFF_BEGIN_DT DATE not null,
EFF_END_DT DATE	not null,
CRE_DT DATE	not null,
Is_Valid_Flag varchar(1),
UPD_TS DATETIME	not null,
ORG_BUSINESS_UNIT_KEY INT identity(1,1) not null)


insert into DIM_ORG_BUSINESS_UNIT_SQL_IN1544
select
iif(ORG_ID is null or ORG_ID =  'NULL','N/A',LTRIM(RTRIM(ORG_ID))) as ORG_ID,
iif(SRC_ORG_CD is null or SRC_ORG_CD =  'NULL','N/A',(LTRIM(RTRIM(SRC_ORG_CD)))) as SRC_ORG_CD,
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(ORG_TYPE_ID)) AS INT)) as ORG_TYPE_ID,
iif(ORG_NM is null or ORG_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_NM))) as ORG_NM,
iif(PARENT_ORG_ID is null or PARENT_ORG_ID =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_ID))) as PARENT_ORG_ID,
iif(PARENT_ORG_NM is null or PARENT_ORG_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_NM))) as PARENT_ORG_NM,
iif(WM_RDC_NUM is null or WM_RDC_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_RDC_NUM))) as WM_RDC_NUM,
iif(WM_STORE_NUM is null or WM_STORE_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_STORE_NUM))) as WM_STORE_NUM,
iif(WM_DSTRBTR_NO is null or WM_DSTRBTR_NO =  'NULL','N/A',LTRIM(RTRIM(WM_DSTRBTR_NO))) as WM_DSTRBTR_NO,
iif(WH_IND is null or WH_IND =  'NULL',101,cast(ltrim(rtrim(wh_ind))AS bigint)) as WH_IND,
iif(DSV_IND is null or DSV_IND =  'NULL',101,cast(ltrim(rtrim(dsv_ind))AS bigint)) as DSV_IND,
iif(ACTV_IND is null or ACTV_IND =  'NULL',101,cast(ltrim(rtrim(actv_ind))AS bigint)) as ACTV_IND,
iif(EFF_BEGIN_DT is null or EFF_BEGIN_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_BEGIN_DT)))) as EFF_BEGIN_DT,
iif(EFF_END_DT is null or EFF_END_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_END_DT)))) as EFF_END_DT,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
1 as IsValidFlag,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(UPD_TS)))) as UPD_TS
from BCMPWMT.ORG_BUSINESS_UNIT

select ORG_TYPE_ID,count(*) from 
(select
iif(ORG_ID is null or ORG_ID =  'NULL','N/A',LTRIM(RTRIM(ORG_ID))) as ORG_ID,
iif(SRC_ORG_CD is null or SRC_ORG_CD =  'NULL','N/A',(LTRIM(RTRIM(SRC_ORG_CD)))) as SRC_ORG_CD,
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(ORG_TYPE_ID)) AS INT)) as ORG_TYPE_ID,
iif(ORG_NM is null or ORG_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_NM))) as ORG_NM,
iif(PARENT_ORG_ID is null or PARENT_ORG_ID =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_ID))) as PARENT_ORG_ID,
iif(PARENT_ORG_NM is null or PARENT_ORG_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_NM))) as PARENT_ORG_NM,
iif(WM_RDC_NUM is null or WM_RDC_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_RDC_NUM))) as WM_RDC_NUM,
iif(WM_STORE_NUM is null or WM_STORE_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_STORE_NUM))) as WM_STORE_NUM,
iif(WM_DSTRBTR_NO is null or WM_DSTRBTR_NO =  'NULL','N/A',LTRIM(RTRIM(WM_DSTRBTR_NO))) as WM_DSTRBTR_NO,
iif(WH_IND is null or WH_IND =  'NULL',101,cast(ltrim(rtrim(wh_ind))AS bigint)) as WH_IND,
iif(DSV_IND is null or DSV_IND =  'NULL',101,cast(ltrim(rtrim(dsv_ind))AS bigint)) as DSV_IND,
iif(ACTV_IND is null or ACTV_IND =  'NULL',101,cast(ltrim(rtrim(actv_ind))AS bigint)) as ACTV_IND,
iif(EFF_BEGIN_DT is null or EFF_BEGIN_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_BEGIN_DT)))) as EFF_BEGIN_DT,
iif(EFF_END_DT is null or EFF_END_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_END_DT)))) as EFF_END_DT,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
1 as IsValidFlag,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(UPD_TS)))) as UPD_TS
from BCMPWMT.ORG_BUSINESS_UNIT)src
group by ORG_TYPE_ID


select ORG_TYPE_ID,count(*) from DIM_ORG_BUSINESS_UNIT_SQL_IN1544
group by ORG_TYPE_ID
having count(*) > 1

select * from DIM_ORG_BUSINESS_UNIT_SQL_IN1544 t left join
(select
iif(ORG_ID is null or ORG_ID =  'NULL','N/A',LTRIM(RTRIM(ORG_ID))) as ORG_ID,
iif(SRC_ORG_CD is null or SRC_ORG_CD =  'NULL','N/A',(LTRIM(RTRIM(SRC_ORG_CD)))) as SRC_ORG_CD,
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(ORG_TYPE_ID)) AS INT)) as ORG_TYPE_ID,
iif(ORG_NM is null or ORG_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_NM))) as ORG_NM,
iif(PARENT_ORG_ID is null or PARENT_ORG_ID =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_ID))) as PARENT_ORG_ID,
iif(PARENT_ORG_NM is null or PARENT_ORG_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_NM))) as PARENT_ORG_NM,
iif(WM_RDC_NUM is null or WM_RDC_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_RDC_NUM))) as WM_RDC_NUM,
iif(WM_STORE_NUM is null or WM_STORE_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_STORE_NUM))) as WM_STORE_NUM,
iif(WM_DSTRBTR_NO is null or WM_DSTRBTR_NO =  'NULL','N/A',LTRIM(RTRIM(WM_DSTRBTR_NO))) as WM_DSTRBTR_NO,
iif(WH_IND is null or WH_IND =  'NULL',101,cast(ltrim(rtrim(wh_ind))AS bigint)) as WH_IND,
iif(DSV_IND is null or DSV_IND =  'NULL',101,cast(ltrim(rtrim(dsv_ind))AS bigint)) as DSV_IND,
iif(ACTV_IND is null or ACTV_IND =  'NULL',101,cast(ltrim(rtrim(actv_ind))AS bigint)) as ACTV_IND,
iif(EFF_BEGIN_DT is null or EFF_BEGIN_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_BEGIN_DT)))) as EFF_BEGIN_DT,
iif(EFF_END_DT is null or EFF_END_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_END_DT)))) as EFF_END_DT,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
1 as IsValidFlag,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(UPD_TS)))) as UPD_TS
from BCMPWMT.ORG_BUSINESS_UNIT)s
on t.ORG_ID = s.ORG_ID
where s.ORG_ID is null or
(t.ORG_ID	 <> 	s.ORG_ID	) or
(t.SRC_ORG_CD	 <> 	s.SRC_ORG_CD	) or
(t.ORG_TYPE_ID	 <> 	s.ORG_TYPE_ID	) or
(t.ORG_NM	 <> 	s.ORG_NM	) or
(t.PARENT_ORG_ID	 <> 	s.PARENT_ORG_ID	) or
(t.PARENT_ORG_NM	 <> 	s.PARENT_ORG_NM	) or
(t.WM_RDC_NUM	 <> 	s.WM_RDC_NUM	) or
(t.WM_STORE_NUM	 <> 	s.WM_STORE_NUM	) or
(t.WM_DSTRBTR_NO	 <> 	s.WM_DSTRBTR_NO	) or
(t.WH_IND	 <> 	s.WH_IND	) or
(t.DSV_IND	 <> 	s.DSV_IND	) or
(t.ACTV_IND	 <> 	s.ACTV_IND	) or
(t.EFF_BEGIN_DT	 <> 	s.EFF_BEGIN_DT	) or
(t.EFF_END_DT	 <> 	s.EFF_END_DT	) or
(t.CRE_DT	 <> 	s.CRE_DT	) or
(t.UPD_TS	 <> 	s.UPD_TS	)


select ORG_ID,ORG_TYPE_ID,ORG_NM from
DIM_ORG_BUSINESS_UNIT_SQL_IN1544
where ORG_ID = 116443

select ORG_ID,ORG_TYPE_ID,ORG_NM from
(select
iif(ORG_ID is null or ORG_ID =  'NULL','N/A',LTRIM(RTRIM(ORG_ID))) as ORG_ID,
iif(SRC_ORG_CD is null or SRC_ORG_CD =  'NULL','N/A',(LTRIM(RTRIM(SRC_ORG_CD)))) as SRC_ORG_CD,
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,cast(ltrim(rtrim(ORG_TYPE_ID)) AS INT)) as ORG_TYPE_ID,
iif(ORG_NM is null or ORG_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_NM))) as ORG_NM,
iif(PARENT_ORG_ID is null or PARENT_ORG_ID =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_ID))) as PARENT_ORG_ID,
iif(PARENT_ORG_NM is null or PARENT_ORG_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_NM))) as PARENT_ORG_NM,
iif(WM_RDC_NUM is null or WM_RDC_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_RDC_NUM))) as WM_RDC_NUM,
iif(WM_STORE_NUM is null or WM_STORE_NUM =  'NULL','N/A',LTRIM(RTRIM(WM_STORE_NUM))) as WM_STORE_NUM,
iif(WM_DSTRBTR_NO is null or WM_DSTRBTR_NO =  'NULL','N/A',LTRIM(RTRIM(WM_DSTRBTR_NO))) as WM_DSTRBTR_NO,
iif(WH_IND is null or WH_IND =  'NULL',101,cast(ltrim(rtrim(wh_ind))AS bigint)) as WH_IND,
iif(DSV_IND is null or DSV_IND =  'NULL',101,cast(ltrim(rtrim(dsv_ind))AS bigint)) as DSV_IND,
iif(ACTV_IND is null or ACTV_IND =  'NULL',101,cast(ltrim(rtrim(actv_ind))AS bigint)) as ACTV_IND,
iif(EFF_BEGIN_DT is null or EFF_BEGIN_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_BEGIN_DT)))) as EFF_BEGIN_DT,
iif(EFF_END_DT is null or EFF_END_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(EFF_END_DT)))) as EFF_END_DT,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
1 as IsValidFlag,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(UPD_TS)))) as UPD_TS
from BCMPWMT.ORG_BUSINESS_UNIT)s
where ORG_ID = 116443

------------------------------------------------------------------------------------------------------------------------------
select * from Dim_ORG_TYPE_LKP_SQL_IN1544
select * from BCMPWMT.ORG_TYPE_LKP

insert into Dim_ORG_TYPE_LKP_SQL_IN1544
select 
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(ORG_TYPE_ID)))) as ORG_TYPE_ID,
iif(ORG_TYPE_CD is null or ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_CD))) as ORG_TYPE_CD,
iif(ORG_TYPE_DESC is null or ORG_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_DESC))) as ORG_TYPE_DESC,
iif(ORG_TYPE_NM is null or ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_NM))) as ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_NM is null or PARENT_ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_NM))) as PARENT_ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_CD is null or PARENT_ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_CD))) as PARENT_ORG_TYPE_CD,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.ORG_TYPE_LKP

select PARENT_ORG_TYPE_NM,count(*) from
(select 
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(ORG_TYPE_ID)))) as ORG_TYPE_ID,
iif(ORG_TYPE_CD is null or ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_CD))) as ORG_TYPE_CD,
iif(ORG_TYPE_DESC is null or ORG_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_DESC))) as ORG_TYPE_DESC,
iif(ORG_TYPE_NM is null or ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_NM))) as ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_NM is null or PARENT_ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_NM))) as PARENT_ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_CD is null or PARENT_ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_CD))) as PARENT_ORG_TYPE_CD,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.ORG_TYPE_LKP)src
group by PARENT_ORG_TYPE_NM

select ORG_TYPE_ID,count(*) from Dim_ORG_TYPE_LKP_SQL_IN1544
group by ORG_TYPE_ID
having count(*) > 1

select * from Dim_ORG_TYPE_LKP_SQL_IN1544 t left join
(select 
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(ORG_TYPE_ID)))) as ORG_TYPE_ID,
iif(ORG_TYPE_CD is null or ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_CD))) as ORG_TYPE_CD,
iif(ORG_TYPE_DESC is null or ORG_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_DESC))) as ORG_TYPE_DESC,
iif(ORG_TYPE_NM is null or ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_NM))) as ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_NM is null or PARENT_ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_NM))) as PARENT_ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_CD is null or PARENT_ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_CD))) as PARENT_ORG_TYPE_CD,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.ORG_TYPE_LKP)s
on t.ORG_TYPE_ID = s.ORG_TYPE_ID
where s.ORG_TYPE_ID is null or
(t.ORG_TYPE_ID	 <> 	s.ORG_TYPE_ID	) or
(t.ORG_TYPE_CD	 <> 	s.ORG_TYPE_CD	) or
(t.ORG_TYPE_DESC	 <> 	s.ORG_TYPE_DESC	) or
(t.ORG_TYPE_NM	 <> 	s.ORG_TYPE_NM	) or
(t.PARENT_ORG_TYPE_NM	 <> 	s.PARENT_ORG_TYPE_NM	) or
(t.PARENT_ORG_TYPE_CD	 <> 	s.PARENT_ORG_TYPE_CD	) or
(t.CRE_DT	 <> 	s.CRE_DT	) or
(t.CRE_USER	 <> 	s.CRE_USER	)


select ORG_TYPE_ID, ORG_TYPE_DESC, PARENT_ORG_TYPE_NM from
(select 
iif(ORG_TYPE_ID is null or ORG_TYPE_ID =  'NULL',101,CONVERT(INT,LTRIM(RTRIM(ORG_TYPE_ID)))) as ORG_TYPE_ID,
iif(ORG_TYPE_CD is null or ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_CD))) as ORG_TYPE_CD,
iif(ORG_TYPE_DESC is null or ORG_TYPE_DESC =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_DESC))) as ORG_TYPE_DESC,
iif(ORG_TYPE_NM is null or ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(ORG_TYPE_NM))) as ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_NM is null or PARENT_ORG_TYPE_NM =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_NM))) as PARENT_ORG_TYPE_NM,
iif(PARENT_ORG_TYPE_CD is null or PARENT_ORG_TYPE_CD =  'NULL','N/A',LTRIM(RTRIM(PARENT_ORG_TYPE_CD))) as PARENT_ORG_TYPE_CD,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',CONVERT(DATE,LTRIM(RTRIM(CRE_DT)))) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',LTRIM(RTRIM(CRE_USER))) as CRE_USER
from BCMPWMT.ORG_TYPE_LKP)s
where ORG_TYPE_ID = 500

select ORG_TYPE_ID, ORG_TYPE_DESC, PARENT_ORG_TYPE_NM from
Dim_ORG_TYPE_LKP_SQL_IN1544
where ORG_TYPE_ID = 500

-------------------------------------------------------------------------------------------------------------------------------------------
select * from BCMPWMT.PROD_RPT_HRCHY

select * from dim_prod_rpt_hrchy_IN1544

insert into dim_prod_rpt_hrchy_IN1544
select
iif(PROD_RPT_HRCHY_ASSOC_ID is null or PROD_RPT_HRCHY_ASSOC_ID =  'NULL',101,cast(ltrim(rtrim(PROD_RPT_HRCHY_ASSOC_ID))as int)) as PROD_RPT_HRCHY_ASSOC_ID,
iif(CATLG_ITEM_ID is null or CATLG_ITEM_ID =  'NULL',101,cast(ltrim(rtrim(catlg_item_id))as int)) as CATLG_ITEM_ID,
iif(RPT_HRCHY_ID is null or RPT_HRCHY_ID =  'NULL',101,cast(ltrim(rtrim(rpt_hrchy_id))as int)) as RPT_HRCHY_ID,
iif(RH_SUB_CATEG_NM is null or RH_SUB_CATEG_NM =  'NULL',101,cast(ltrim(rtrim(rh_sub_categ_nm))as int)) as RH_SUB_CATEG_NM,
iif(CURR_IND is null or CURR_IND =  'NULL',101,cast(ltrim(rtrim(CURR_IND))as INT)) as CURR_IND,
iif(EFF_BEGIN_DT is null or EFF_BEGIN_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(eff_begin_dt))as date)) as EFF_BEGIN_DT,
iif(EFF_END_DT is null or EFF_END_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(eff_end_dt))as date)) as EFF_END_DT,
iif(PRMRY_CATEG_PATH is null or PRMRY_CATEG_PATH =  'NULL','N/A',ltrim(rtrim(PRMRY_CATEG_PATH))) as PRMRY_CATEG_PATH,
iif(CHAR_PRMRY_CATEG_PATH is null or CHAR_PRMRY_CATEG_PATH =  'NULL','N/A',ltrim(rtrim(CHAR_PRMRY_CATEG_PATH))) as CHAR_PRMRY_CATEG_PATH,
iif(RH_SUB_CATEG_ID is null or RH_SUB_CATEG_ID =  'NULL',101,cast(ltrim(rtrim(RH_SUB_CATEG_ID))as int)) as RH_SUB_CATEG_ID,
iif(PRMRY_SHELF_ID is null or PRMRY_SHELF_ID =  'NULL',101,cast(ltrim(rtrim(PRMRY_SHELF_ID))as int)) as PRMRY_SHELF_ID,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(CRE_DT))as date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',cast(ltrim(rtrim(CRE_USER))as varchar)) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(UPD_TS))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',cast(ltrim(rtrim(UPD_USER))as varchar)) as UPD_USER
from BCMPWMT.PROD_RPT_HRCHY



alter table  IN1544.dim_prod_rpt_hrchy_IN1544 add RPT_HRCHY_KEY int
update IN1544.dim_prod_rpt_hrchy_IN1544 set RPT_HRCHY_KEY= pr.RPT_HRCHY_KEY from [DIM_PROD_RPT_HRCHY] p 
join dim_rpt_hrchy pr on  
convert(varchar(50),p.[RPT_HRCHY_ID]) = convert(varchar(50),pr.[RPT_HRCHY_ID])
) as RPT_HRCHY_KEY,

create table dim_prod_rpt_hrchy_IN1544
(prod_rpt_key int identity(1,1) primary key not null,		
PROD_RPT_HRCHY_ASSOC_ID int	not null,
CATLG_ITEM_ID int not null,
RPT_HRCHY_ID int not null,
RH_SUB_CATEG_NM	int	not null,
CURR_IND int not null,
EFF_BEGIN_DT date not null,
EFF_END_DT date	not null,
PRMRY_CATEG_PATH varchar(200) not null,
CHAR_PRMRY_CATEG_PATH varchar(200) not null,
RH_SUB_CATEG_ID int	not null,
PRMRY_SHELF_ID int not null,
CRE_DT date	not null,
CRE_USER varchar(50) not null,
UPD_TS datetime	not null,
UPD_USER varchar(50) not null)

drop table dim_prod_rpt_hrchy_IN1544


-------------------------------------------------------------------------------------------------------------------------------

select * from BCMPWMT.RPT_HRCHY
select * from dim_rpt_hrchy_SQL_IN1544

select
iif(RPT_HRCHY_ID is null or RPT_HRCHY_ID =  'NULL',101,cast((RPT_HRCHY_ID) as FLOAT)) as RPT_HRCHY_ID,
iif(SRC_RPT_HRCHY_ID is null or SRC_RPT_HRCHY_ID =  'NULL',101,cast(src_rpt_hrchy_id as FLOAT)) as SRC_RPT_HRCHY_ID,
iif(TENANT_ORG_ID is null or TENANT_ORG_ID =  'NULL','N/A',LTRIM(RTRIM(tenant_org_id))) as TENANT_ORG_ID,
iif(RPT_HRCHY_PATH is null or RPT_HRCHY_PATH =  'NULL','N/A',cast(ltrim(rtrim(RPT_HRCHY_PATH)) as varchar)) as RPT_HRCHY_PATH,
iif(DIV_ID is null or DIV_ID =  'NULL',101,cast(div_id as FLOAT)) as DIV_ID,
iif(DIV_NM is null or DIV_NM =  'NULL','N/A',cast(ltrim(rtrim(div_nm)) as varchar)) as DIV_NM,
iif(SUPER_DEPT_ID is null or SUPER_DEPT_ID =  'NULL',101,cast(super_dept_id as int)) as SUPER_DEPT_ID,
iif(SUPER_DEPT_NM is null or SUPER_DEPT_NM =  'NULL','N/A',cast(ltrim(rtrim(super_dept_nm)) as varchar)) as SUPER_DEPT_NM,
iif(DEPT_ID is null or DEPT_ID =  'NULL',101,cast(dept_id as int)) as DEPT_ID,
iif(DEPT_NM is null or DEPT_NM =  'NULL','N/A',cast(ltrim(rtrim(dept_nm)) as varchar)) as DEPT_NM,
iif(CATEG_NM is null or CATEG_NM =  'NULL','N/A',cast(ltrim(rtrim(categ_nm)) as varchar)) as CATEG_NM,
iif(SUB_CATEG_ID is null or SUB_CATEG_ID =  'NULL',101,cast(sub_categ_id as int)) as SUB_CATEG_ID,
iif(SUB_CATEG_NM is null or SUB_CATEG_NM =  'NULL','N/A',cast(ltrim(rtrim(sub_categ_nm)) as varchar)) as SUB_CATEG_NM,
iif(ITEM_CATEG_GROUPING_ID is null or ITEM_CATEG_GROUPING_ID =  'NULL','N/A',cast(ltrim(rtrim(item_categ_grouping_id)) as varchar)) as ITEM_CATEG_GROUPING_ID,
iif(SRC_CRE_TS is null or SRC_CRE_TS =  'NULL','N/A',SRC_CRE_TS) as SRC_CRE_TS,
iif(SRC_MODFD_TS is null or SRC_MODFD_TS =  'NULL','N/A',SRC_MODFD_TS) as SRC_MODFD_TS,
iif(SRC_HRCHY_MODFD_TS is null or SRC_HRCHY_MODFD_TS =  'NULL','01-01-1900',SRC_HRCHY_MODFD_TS) as SRC_HRCHY_MODFD_TS,
iif(CATEG_MGR_NM is null or CATEG_MGR_NM =  'NULL','N/A',cast(ltrim(rtrim(categ_mgr_nm)) as varchar)) as CATEG_MGR_NM,
iif(BUYER_NM is null or BUYER_NM =  'NULL','N/A',cast(ltrim(rtrim(buyer_nm)) as varchar)) as BUYER_NM,
iif(EFF_BEGIN_DT is null or EFF_BEGIN_DT =  'NULL','01-01-1900',cast(eff_begin_dt as date)) as EFF_BEGIN_DT,
iif(EFF_END_DT is null or EFF_END_DT =  'NULL','01-01-1900',cast(eff_end_dt as date)) as EFF_END_DT,
iif(RPT_HRCHY_ID_PATH is null or RPT_HRCHY_ID_PATH =  'NULL','N/A',cast(ltrim(rtrim(rpt_hrchy_id_path)) as varchar)) as RPT_HRCHY_ID_PATH,
iif(CATEG_ID is null or CATEG_ID =  'NULL',101,cast(categ_id as int)) as CATEG_ID,
iif(CONSUMABLE_IND is null or CONSUMABLE_IND =  'NULL','N/A',cast(ltrim(rtrim(consumable_ind))as INT)) as CONSUMABLE_IND,
iif(CURR_IND is null or CURR_IND =  'NULL',101,cast(ltrim(rtrim(CURR_IND))as FLOAT)) as CURR_IND,
iif(CRE_DT is null or CRE_DT =  'NULL','01-01-1900',cast(ltrim(rtrim(cre_dt))as date)) as CRE_DT,
iif(CRE_USER is null or CRE_USER =  'NULL','N/A',CRE_USER) as CRE_USER,
iif(UPD_TS is null or UPD_TS =  'NULL','01-01-1900',cast(ltrim(rtrim(upd_ts))as datetime)) as UPD_TS,
iif(UPD_USER is null or UPD_USER =  'NULL','N/A',UPD_USER) as UPD_USER
from BCMPWMT.RPT_HRCHY

select 
iif(RPT_HRCHY_ID is null or RPT_HRCHY_ID =  'NULL',101,cast((RPT_HRCHY_ID) as FLOAT)) as RPT_HRCHY_ID
from BCMPWMT.RPT_HRCHY

----------------------------------------------------------------------------------------------------------------------------