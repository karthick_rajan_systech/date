import React, { useState } from "react";
import { useNavigate } from "react-router-dom";





export const Login = (props) => {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const navigate = useNavigate()
    const handleSubmit = (e) => {
        e.preventDefault();


        var data = localStorage.getItem("data")
        var pData = JSON.parse(data)

        if (pData.email === email && pData.password === pass) {
            navigate('/welcome')
        }
        else {
            alert("Email or password is incorrect!")
        }




    }

    return (
        <div className="auth-form-container">
            <h2>Login</h2>
            <form className="login-form" onSubmit={handleSubmit}>
                <label htmlFor="email">email</label>
                <input value={email} required onChange={(e) => setEmail(e.target.value)} type="email" placeholder="youremail@gmail.com" id="email" name="email" />
                <label htmlFor="password">password</label>
                <input value={pass} required onChange={(e) => setPass(e.target.value)} type="password" placeholder="********" id="password" name="password" />
                <br></br>
                <button type="submit">Log In</button>
            </form>
            <button className="link-btn" onClick={() => navigate('/register')}>Don't have an account? <br></br>Register here.</button>

        </div>
    )
}
