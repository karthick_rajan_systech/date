import React, { useState } from "react";
import { Link, Outlet } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export const Register = (props) => {
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [date, setDate] = useState('');
    const [pass, setPass] = useState('');
    const [name, setName] = useState('');
    const navigate = useNavigate()

    const handleSubmit = (e) => {
        
        var data = {
            name:name,
            date:date,
            phone:phone,
            email:email,
            password:pass
        }
        data = JSON.stringify(data)
        localStorage.setItem("data", data)
        navigate('/welcome')
    }

    return (
        <div className="auth-form-container">
            <h2>Register</h2>
        <form className="register-form" onSubmit={handleSubmit}>
            <label htmlFor="name">Full name</label>
            <input value={name} required onChange={(e) => setName(e.target.value)}type="name" name="name" id="name" placeholder="full Name" />
            <label htmlFor="date">DOB</label>
            <input value={date} required onChange={(e) => setDate(e.target.value)}type="date" name="date" id="date" placeholder="Date of birth" />
            <label htmlFor="phone">Phone Number</label>
            <input value={phone} required onChange={(e) => setPhone(e.target.value)}type="phone" name="phone" id="phone" placeholder="+91" />
            <label htmlFor="email">email</label>
            <input value={email} required onChange={(e) => setEmail(e.target.value)}type="email" placeholder="youremail@gmail.com" id="email" name="email" />
            <label htmlFor="password">password</label>
            <input value={pass} required onChange={(e) => setPass(e.target.value)} type="password" placeholder="********" id="password" name="password" />
            <br />
            <Link to="/welcome"><button type="submit">Log In</button></Link>
        </form>
        <button className="link-btn" onClick={() => navigate('/')}>Already have an account?
        <br></br>
         Login here.</button>
        <Outlet />
    </div>
    )
}
