import React from "react"


 export const Welcome=() => {

  var data = localStorage.getItem("data")
  var parsedData = JSON.parse(data)
      
    return(
      <div> 
        <h1>Welcome!<br></br>
          {parsedData.name}
        </h1>
    
        <h1>
          {parsedData.email}
        </h1>
      </div>  )
  }
